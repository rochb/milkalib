package me.sharkz.milkalib.core.storage.implementations.sql.remote;

import com.zaxxer.hikari.HikariConfig;
import me.sharkz.milkalib.core.storage.StorageCredentials;
import me.sharkz.milkalib.core.storage.StorageType;

import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PostgreConnectionFactory extends HikariConnectionFactory {

    public PostgreConnectionFactory(StorageCredentials credentials) {
        super(credentials);
    }

    @Override
    public StorageType getType() {
        return StorageType.POSTGRE;
    }

    @Override
    public void configure(HikariConfig config, StorageCredentials credentials) {
        config.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
        config.addDataSourceProperty("serverName", credentials.getAddress());
        config.addDataSourceProperty("portNumber", credentials.getPort());
        config.addDataSourceProperty("databaseName", credentials.getDatabase());
        config.addDataSourceProperty("user", credentials.getUsername());
        config.addDataSourceProperty("password", credentials.getPassword());
    }

    @Override
    protected void overrideProperties(Map<String, String> properties) {
        super.overrideProperties(properties);

        // remove the default config properties which don't exist for PostgreSQL
        properties.remove("useUnicode");
        properties.remove("characterEncoding");
    }
}
