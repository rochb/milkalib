package me.sharkz.milkalib.core.exceptions;

/**
 * @author Roch Blondiaux
 */
public class CommandUnregistrationException extends Exception {

    public CommandUnregistrationException(String message) {
        super(message);
    }

    public CommandUnregistrationException(String message, Throwable cause) {
        super(message, cause);
    }
}
