package me.sharkz.milkalib.core.exceptions;

/**
 * @author Roch Blondiaux
 */
public class CommandRegistrationException extends Exception {

    public CommandRegistrationException(String message) {
        super(message);
    }

    public CommandRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }
}
