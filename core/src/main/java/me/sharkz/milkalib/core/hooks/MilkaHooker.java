package me.sharkz.milkalib.core.hooks;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class MilkaHooker implements PluginHooker {

    private boolean hooked;

    @Override
    public void onHookSuccess() {
        this.hooked = true;
    }

    @Override
    public void onHookFail() {
        this.hooked = false;
    }

    @Override
    public boolean isHooked() {
        return hooked;
    }
}
