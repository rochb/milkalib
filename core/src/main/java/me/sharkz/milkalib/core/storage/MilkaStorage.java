package me.sharkz.milkalib.core.storage;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.storage.implementations.sql.ConnectionFactory;
import me.sharkz.milkalib.core.utils.storage.ResultSetHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class MilkaStorage {

    private final MilkaPlugin plugin;

    @Getter
    private final ConnectionFactory connectionFactory;

    public void init() {
        try {
            connectionFactory.init();
            if (getConnectionFactory().getConnection().isValid(5))
                plugin.getMilkaLogger().success("Successfully connected to database!");
            else
                plugin.getMilkaLogger().error("Unable to connect to the database!");
        } catch (Exception e) {
            plugin.getMilkaLogger().error("Unable to connect to the database:");
            e.printStackTrace();
            plugin.getMilkaLogger().info("Disabling plugin...");
            plugin.disable();
        }
    }

    public void shutdown() {
        try {
            connectionFactory.shutdown();
        } catch (SQLException throwable) {
            plugin.getMilkaLogger().warn("Unable to close database connection:");
            throwable.printStackTrace();
        }
    }

    public void execute(String query) {
        try (Connection c = connectionFactory.getConnection()) {
            try (PreparedStatement statement = c.prepareStatement(query)) {
                statement.execute();
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }

    public void executeAsync(String query) {
        plugin.getSchedulerAdapter().executeAsync(() -> execute(query));
    }

    public <T> Optional<T> executeQuery(String query, ResultSetHandler<T> handler) {
        Optional<T> result = Optional.empty();
        try (Connection c = connectionFactory.getConnection()) {
            try (PreparedStatement statement = c.prepareStatement(query)) {
                try (ResultSet rs = statement.executeQuery()) {
                    result = Optional.ofNullable(handler.handleResultSet(rs));
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return result;
    }

    public <T> CompletableFuture<Optional<T>> executeAsyncQuery(String query, ResultSetHandler<T> handler) {
        CompletableFuture<Optional<T>> result = new CompletableFuture<>();
        plugin.getSchedulerAdapter().executeAsync(() -> {
            try (Connection c = connectionFactory.getConnection()) {
                try (PreparedStatement statement = c.prepareStatement(query)) {
                    try (ResultSet rs = statement.executeQuery()) {
                        result.complete(Optional.ofNullable(handler.handleResultSet(rs)));
                    } catch (SQLException throwable) {
                        throwable.printStackTrace();
                    }
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            } catch (SQLException throwable) {
                result.completeExceptionally(throwable);
                throwable.printStackTrace();
            }
        });
        return result;
    }

    public void executeUpdate(String query) {
        try (Connection c = connectionFactory.getConnection()) {
            try (PreparedStatement statement = c.prepareStatement(query)) {
                statement.executeUpdate();
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }

    public void executeAsyncUpdate(String query) {
        plugin.getSchedulerAdapter().executeAsync(() -> executeUpdate(query));
    }

    public StorageType getType() {
        return connectionFactory.getType();
    }
}
