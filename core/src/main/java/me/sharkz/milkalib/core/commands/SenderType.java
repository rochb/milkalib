package me.sharkz.milkalib.core.commands;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum SenderType {
    CONSOLE,
    PLAYER,
    BOTH;
}
