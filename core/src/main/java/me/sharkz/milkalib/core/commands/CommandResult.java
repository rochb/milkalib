package me.sharkz.milkalib.core.commands;

import lombok.Getter;
import me.sharkz.milkalib.core.translations.integrated.CommandsEn;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public enum CommandResult {
    SUCCESS(null),
    FORBIDDEN(CommandsEn.FORBIDDEN),
    ERROR(CommandsEn.COMMAND_ERROR),
    MISSING_ARGUMENT(CommandsEn.COMMAND_MISSING_ARGUMENT),
    INVALID_ARGUMENT_TYPE(CommandsEn.COMMAND_INVALID_ARGUMENT_TYPE),
    INVALID_SYNTAX(CommandsEn.COMMAND_INVALID_SYNTAX),
    UNKNOWN_COMMAND(CommandsEn.COMMAND_UNKNOWN_COMMAND),
    UNSUPPORTED_SENDER(CommandsEn.UNSUPPORTED_SENDER);

    private final CommandsEn message;

    CommandResult(CommandsEn message) {
        this.message = message;
    }

    public boolean hasMessage() {
        return Objects.nonNull(message);
    }
}
