package me.sharkz.milkalib.core.storage.implementations;

import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.storage.StorageType;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface StorageImplementation {

    MilkaPlugin getPlugin();

    StorageType getType();

    void init() throws Exception;

    void shutdown();

}
