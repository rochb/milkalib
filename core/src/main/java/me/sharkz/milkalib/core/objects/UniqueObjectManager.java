package me.sharkz.milkalib.core.objects;

import me.sharkz.milkalib.core.MilkaPlugin;

import java.util.Optional;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class UniqueObjectManager<T extends Unique> extends ObjectManager<T> {

    public UniqueObjectManager(MilkaPlugin plugin) {
        super(plugin);
    }

    /**
     * Get object by it's unique id
     *
     * @param uniqueId object's unique id
     * @return optional of object
     */
    public Optional<T> getByUniqueId(UUID uniqueId) {
        return getObjects().stream().filter(t -> t.getUniqueId().equals(uniqueId)).findFirst();
    }
}
