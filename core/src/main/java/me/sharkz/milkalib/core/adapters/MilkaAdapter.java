package me.sharkz.milkalib.core.adapters;

import java.util.Collection;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface MilkaAdapter<T> {

    /**
     * Parse data from string
     *
     * @param data to parse
     * @return optional of parsed data
     */
    Optional<T> parse(String data);

    /**
     * Get string collection of suggestions for tab completion
     *
     * @return string collection
     */
    Collection<String> getSuggestions();
}
