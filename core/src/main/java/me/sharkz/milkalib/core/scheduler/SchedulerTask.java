package me.sharkz.milkalib.core.scheduler;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface SchedulerTask {

    /**
     * Cancels the task.
     */
    void cancel();

}