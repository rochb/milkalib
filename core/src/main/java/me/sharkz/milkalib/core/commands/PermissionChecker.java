package me.sharkz.milkalib.core.commands;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface PermissionChecker {

    boolean hasPermission(String permission);
}
