package me.sharkz.milkalib.core.adapters.integrated;

import me.sharkz.milkalib.core.adapters.MilkaAdapter;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class DoubleAdapter implements MilkaAdapter<Double> {


    @Override
    public Optional<Double> parse(String data) {
        try {
            return Optional.of(Double.parseDouble(data));
        } catch (Exception ignored) {
            return Optional.empty();
        }
    }

    @Override
    public Collection<String> getSuggestions() {
        return Arrays.asList("0.0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0", "1.5", "2.0", "2.5", "3.0");
    }
}
