package me.sharkz.milkalib.core.hooks;

import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.objects.ObjectManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class HookersManager extends ObjectManager<PluginHooker> {

    public HookersManager(MilkaPlugin plugin) {
        super(plugin);
    }

    /**
     * Check all plugin hookers
     *
     * @return need to disable plugin
     */
    public boolean check() {
        AtomicBoolean needDisable = new AtomicBoolean(false);
        getObjects()
                .forEach(pluginHooker -> {
                    if (isHooked(pluginHooker)) pluginHooker.onHookSuccess();
                    else {
                        pluginHooker.onHookFail();
                        if (pluginHooker.disablePluginIfNotFound())
                            needDisable.set(true);
                    }
                });
        if (needDisable.get()) getPlugin().disable();
        return needDisable.get();
    }

    /**
     * Check if a hooker is hooked with native method (spigot/bungee)
     *
     * @param pluginHooker targeted plugin hooker
     * @return hook state
     */
    protected abstract boolean isHooked(PluginHooker pluginHooker);

    /**
     * Get hooker by it's class
     *
     * @param hookerClass hooker's class
     * @param <T>         hooker's type
     * @return optional of hooker instance
     */
    public <T extends PluginHooker> Optional<T> getByClass(Class<? extends PluginHooker> hookerClass) {
        return (Optional<T>) getObjects().stream().filter(pluginHooker -> pluginHooker.getClass().equals(hookerClass)).findFirst();
    }

    /**
     * Check if an hooker is hooked by it's class
     *
     * @param hookerClass hooker's class
     * @return hook state
     */
    public boolean isHooked(Class<? extends PluginHooker> hookerClass) {
        return getByClass(hookerClass).map(PluginHooker::isHooked).orElse(false);
    }

    /**
     * Register plugin hooker by it's class
     *
     * @param hookerClass          hooker's class
     * @param constructorArguments hooker's constructor arguments
     */
    public void register(Class<? extends PluginHooker> hookerClass, Object... constructorArguments) {
        try {
            Constructor<? extends PluginHooker> constructor = hookerClass.getDeclaredConstructor(Arrays.stream(constructorArguments).map(Object::getClass).collect(Collectors.toList()).toArray(new Class[]{}));
            PluginHooker hooker = constructor.newInstance(constructorArguments);
            add(hooker);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
