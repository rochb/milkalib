package me.sharkz.milkalib.core.hooks.integrated;

import me.sharkz.milkalib.core.hooks.MilkaHooker;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class VaultHook extends MilkaHooker {

    private final boolean disableIfNotFound;

    public VaultHook(boolean disableIfNotFound) {
        this.disableIfNotFound = disableIfNotFound;
    }

    @Override
    public String getRequiredPlugin() {
        return "Vault";
    }

    @Override
    public boolean disablePluginIfNotFound() {
        return disableIfNotFound;
    }
}
