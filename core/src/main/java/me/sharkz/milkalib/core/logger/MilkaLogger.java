package me.sharkz.milkalib.core.logger;

import me.sharkz.milkalib.core.MilkaPlugin;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface MilkaLogger {

    void error(String message);

    void info(String message);

    void warn(String message);

    void success(String message);

    void debug(String message);

    void error(String message, Throwable throwable);

    void warn(String message, Throwable throwable);

    boolean isDebugEnabled();

    void setDebugEnabled(boolean debug);

    MilkaPlugin getPlugin();

}
