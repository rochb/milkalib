package me.sharkz.milkalib.core.adapters;

import lombok.AllArgsConstructor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@AllArgsConstructor
public class AdaptersManager {

    private final List<MilkaAdapter<?>> types = new ArrayList<>();

    /**
     * Get argument adapter by it's type
     *
     * @param type argument adapter's type
     * @return
     */
    public Optional<MilkaAdapter<?>> getAdapter(Class<?> type) {
        return types.stream()
                .filter(argumentAdapter -> getGenericClass(argumentAdapter).equals(type))
                .findFirst();

    }

    public boolean isRegistered(Class<?> type) {
        return types.stream().anyMatch(argumentAdapter -> getGenericClass(argumentAdapter).equals(type));
    }

    public void register(Class<? extends MilkaAdapter<?>> adapter, Object... constructorArguments) {
        if (isRegistered(adapter)) return;
        try {
            Constructor<? extends MilkaAdapter<?>> constructor = adapter.getDeclaredConstructor(Arrays.stream(constructorArguments).map(Object::getClass).collect(Collectors.toList()).toArray(new Class[]{}));
            MilkaAdapter<?> a = constructor.newInstance(constructorArguments);
            types.add(a);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void register(Class<? extends MilkaAdapter<?>> adapter) {
        if (isRegistered(adapter)) return;
        try {
            MilkaAdapter<?> a = adapter.newInstance();
            types.add(a);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void register(Class<? extends MilkaAdapter<?>>... adapters) {
        for (Class<? extends MilkaAdapter<?>> adapter : adapters) register(adapter);
    }

    public void unregister(Class<? extends MilkaAdapter<?>> adapter) {
        types.removeIf(adapter1 -> getGenericClass(adapter1).equals(adapter));
    }

    public <T> Class<T> getGenericClass(MilkaAdapter<?> serializer) {
        return (Class<T>) ((ParameterizedType) serializer.getClass().getGenericInterfaces()[0]).getActualTypeArguments()[0];
    }
}
