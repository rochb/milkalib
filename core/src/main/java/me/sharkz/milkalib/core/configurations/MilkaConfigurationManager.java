package me.sharkz.milkalib.core.configurations;

import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.configurations.wrapper.YamlFileLoadException;
import me.sharkz.milkalib.core.configurations.wrapper.YmlFileWrapper;
import me.sharkz.milkalib.core.objects.ObjectManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class MilkaConfigurationManager<T extends YmlFileWrapper> extends ObjectManager<T> {

    public MilkaConfigurationManager(MilkaPlugin plugin) {
        super(plugin);
    }

    /**
     * Get YmlFileWrapper by it's class
     *
     * @param wrapperClass wrapper's class
     * @return optional of YmlFileWrapper
     */
    public Optional<T> getByClass(Class<? extends YmlFileWrapper> wrapperClass) {
        return getObjects().stream().filter(t -> t.getClass().equals(wrapperClass)).findFirst();
    }

    /**
     * Register YmlFileWrapper by it's class
     *
     * @param wrapperClass    wrapper's class
     * @param constructorArgs wrapper's constructor arguments
     * @return wrapper's instance
     */
    public T register(Class<? extends YmlFileWrapper> wrapperClass, Object... constructorArgs) {
        try {
            Class[] classes = Arrays.stream(constructorArgs).map(Object::getClass).collect(Collectors.toList()).toArray(new Class[]{});
            Constructor<? extends YmlFileWrapper> wrapperConstructor = wrapperClass.getDeclaredConstructor(classes);
            T wrapper = (T) wrapperConstructor.newInstance(constructorArgs);
            add(wrapper);
            return wrapper;
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Add and load YmlFileWrapper
     *
     * @param object object to add
     */
    @Override
    public void add(T object) {
        try {
            object.load();
            super.add(object);
        } catch (YamlFileLoadException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load all YmlFileWrappers from files
     */
    public void loadAll() {
        getObjects().forEach(t -> {
            try {
                t.load();
            } catch (YamlFileLoadException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Save all YmlFileWrappers to files
     */
    public void saveAll() {
        getObjects().forEach(t -> {
            try {
                t.load();
            } catch (YamlFileLoadException e) {
                e.printStackTrace();
            }
        });
    }
}
