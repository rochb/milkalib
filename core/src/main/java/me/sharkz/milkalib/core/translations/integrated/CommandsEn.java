package me.sharkz.milkalib.core.translations.integrated;

import me.sharkz.milkalib.core.translations.Translation;
import me.sharkz.milkalib.core.translations.TranslationManager;
import me.sharkz.milkalib.core.translations.TranslationPack;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum CommandsEn implements TranslationPack {
    FORBIDDEN("&c&l!! &eYou don't have right permission to do that!"),
    COMMAND_ERROR("&c&l!! &eAn error occurred while executing command! Please contact an administrator."),
    COMMAND_MISSING_ARGUMENT("&c&l!! &eAn argument is missing! Usage: %syntax%"),
    COMMAND_INVALID_ARGUMENT_TYPE("&c&l!! &eWrong argument type! Usage: %syntax%"),
    COMMAND_INVALID_SYNTAX("&c&l!! &eInvalid command syntax! Usage: %syntax%"),
    COMMAND_UNKNOWN_COMMAND("&c&l!! &eUnknown command! &7Type &e/help&7 to get the help menu."),
    UNSUPPORTED_SENDER("&c&l!! &eThis command only support %sender% !"),
    PLUGIN_RELOADED_SUCCESSFULLY("&a&l!! &7Plugin has been reloaded successfully!");

    private final String content;
    private TranslationManager manager;

    CommandsEn(String content) {
        this.content = content;
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), Collections.singletonList(this.content));
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(CommandsEn::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(TranslationManager manager) {
        this.manager = manager;
    }

    @Override
    public TranslationManager getManager() {
        return this.manager;
    }

    public List<String> toList() {
        return this.get().translate();
    }
}
