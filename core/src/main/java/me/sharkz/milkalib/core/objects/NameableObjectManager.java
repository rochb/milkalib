package me.sharkz.milkalib.core.objects;

import me.sharkz.milkalib.core.MilkaPlugin;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class NameableObjectManager<T extends Nameable> extends ObjectManager<T> {

    public NameableObjectManager(MilkaPlugin plugin) {
        super(plugin);
    }

    /**
     * Get object by it's exact name
     *
     * @param name object's name
     * @return optional of object
     */
    public Optional<T> getByName(String name) {
        return getObjects().stream().filter(t -> t.getName().equals(name)).findFirst();
    }

    /**
     * Get object by it's name ignore case
     *
     * @param name object's name
     * @return optional of object
     */
    public Optional<T> getByNameIgnoreCase(String name) {
        return getObjects().stream().filter(t -> t.getName().equalsIgnoreCase(name)).findFirst();
    }
}
