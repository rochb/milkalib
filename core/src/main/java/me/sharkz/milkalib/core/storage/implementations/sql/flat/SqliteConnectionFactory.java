package me.sharkz.milkalib.core.storage.implementations.sql.flat;

import me.sharkz.milkalib.core.storage.StorageType;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SqliteConnectionFactory extends FlatFileConnectionFactory {


    public SqliteConnectionFactory(Path file) {
        super(file);
    }

    @Override
    public StorageType getType() {
        return StorageType.SQLITE;
    }

    @Override
    protected Connection createConnection(Path file) throws SQLException {
        try {
            Class.forName("org.sqlite.JDBC");
            return DriverManager.getConnection("jdbc:sqlite:" + getWriteFile().toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected Path getWriteFile() {
        Path writeFile = super.getWriteFile();
        return writeFile.getParent().resolve(writeFile.getFileName().toString() + ".db");
    }
}
