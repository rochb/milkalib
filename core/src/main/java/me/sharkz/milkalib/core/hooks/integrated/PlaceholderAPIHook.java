package me.sharkz.milkalib.core.hooks.integrated;

import me.sharkz.milkalib.core.hooks.MilkaHooker;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PlaceholderAPIHook extends MilkaHooker {

    private final boolean disableIfNotFound;

    public PlaceholderAPIHook(boolean disableIfNotFound) {
        this.disableIfNotFound = disableIfNotFound;
    }

    @Override
    public String getRequiredPlugin() {
        return "PlaceholderAPI";
    }

    @Override
    public boolean disablePluginIfNotFound() {
        return disableIfNotFound;
    }

}
