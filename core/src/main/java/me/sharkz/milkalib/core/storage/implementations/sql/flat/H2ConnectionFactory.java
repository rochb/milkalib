package me.sharkz.milkalib.core.storage.implementations.sql.flat;

import me.sharkz.milkalib.core.storage.StorageType;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class H2ConnectionFactory extends FlatFileConnectionFactory {

    public H2ConnectionFactory(Path file) {
        super(file);
    }

    @Override
    public StorageType getType() {
        return StorageType.H2;
    }

    @Override
    protected Connection createConnection(Path file) throws SQLException {
        try {
            Class.forName("org.h2.Driver");
            return DriverManager.getConnection("jdbc:h2:" + getWriteFile().relativize(getWriteFile().getParent()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected Path getWriteFile() {
        Path writeFile = super.getWriteFile();
        return writeFile.getParent().resolve(writeFile.getFileName().toString() + ".mv.db");
    }
}
