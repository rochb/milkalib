package me.sharkz.milkalib.core.dependencies;

import lombok.Getter;
import me.sharkz.milkalib.core.dependencies.classloader.LoaderType;

import java.util.Arrays;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public enum SQLDependencies {
    SQLITE_DRIVER(
            "org.xerial",
            "sqlite-jdbc",
            "3.32.3.2",
            "7CL07opEtcliARxo7Ocu5TeojrvnytcNP4fIFq6lM1c=",
            LoaderType.ISOLATED
    ),
    HIKARI_DRIVER(
            "com.zaxxer",
            "HikariCP",
            "4.0.3",
            "fAJK7/HBBjV210RTUT+d5kR9jmJNF/jifzCi6XaIxsk=",
            LoaderType.ISOLATED
    ),
    MARIADB_DRIVER(
            "org.mariadb.jdbc",
            "mariadb-java-client",
            "2.7.2",
            "o/Z3bfCELPZefxWFFQEtUwfalJ9mBCKC4e5EdN0Z9Eg=",
            LoaderType.ISOLATED
    ),
    POSTGRESQL_DRIVER(
            "org.postgresql",
            "postgresql",
            "42.2.18",
            "DIkZefHrL+REMtoRTQl2C1Bj2tnmaawKxrC2v7kbs7o=",
            LoaderType.ISOLATED
    ),
    MYSQL_DRIVER(
            "mysql",
            "mysql-connector-java",
            "8.0.24",
            "mbUtpj2dMdVJSjAvPbtVS3MMwg2x+wntpkqw4nwTCqs=",
            LoaderType.ISOLATED
    ),
    H2_DRIVER(
            "com.h2database",
            "h2",
            "1.4.200",
            "OtmsS2qunNnTrBxEdGXh7QYBm4UbiT3WqNdt222FvKY=",
            LoaderType.ISOLATED
    );

    private final Dependency dependency;

    SQLDependencies(String groupId, String artifactId, String version, String checksum, LoaderType loaderType) {
        this.dependency = new Dependency(name(), groupId, artifactId, version, checksum, loaderType);
    }

    public static Dependency[] get() {
        return Arrays.stream(values()).map(SQLDependencies::getDependency).toArray(Dependency[]::new);
    }
}
