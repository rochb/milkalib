package me.sharkz.milkalib.core.configurations.wrapper;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface YmlFileWrapper {

    /**
     * Loads the configuration file from the disk.
     * Create the file on the disk with the default resource if provided.
     *
     * @throws YamlFileLoadException thrown if the configuration file cannot be loaded
     */
    void load() throws YamlFileLoadException;

    /**
     * Saves the configuration file on the disk.
     *
     * @throws IOException thrown if the configuration file cannot be saved
     */
    void save() throws IOException;

    /**
     * Check if file exists
     *
     * @return true if it's exists
     */
    boolean exists();

    /**
     * Creates a stream with the content of the default resource.
     *
     * @return input stream with the resource content
     */
    InputStream getDefaultResourceStream() throws IOException;

}
