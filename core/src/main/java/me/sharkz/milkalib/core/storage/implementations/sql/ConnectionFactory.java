package me.sharkz.milkalib.core.storage.implementations.sql;

import com.zaxxer.hikari.HikariConfig;
import me.sharkz.milkalib.core.storage.StorageCredentials;
import me.sharkz.milkalib.core.storage.StorageType;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface ConnectionFactory {

    StorageType getType();

    void init() throws SQLException;

    void shutdown() throws SQLException;

    Connection getConnection() throws SQLException;

    void postInitialize();

    void configure(HikariConfig config, StorageCredentials credentials);
}
