package me.sharkz.milkalib.core.hooks.integrated;

import me.sharkz.milkalib.core.hooks.MilkaHooker;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ProtocolLibHook extends MilkaHooker {

    private final boolean disableIfNotFound;

    public ProtocolLibHook(Boolean disableIfNotFound) {
        this.disableIfNotFound = disableIfNotFound;
    }

    @Override
    public String getRequiredPlugin() {
        return "ProtocolLib";
    }

    @Override
    public boolean disablePluginIfNotFound() {
        return disableIfNotFound;
    }

}
