package me.sharkz.milkalib.core.objects;

import lombok.Getter;
import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.utils.MilkaUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class ObjectManager<T> extends MilkaUtils {

    @Getter
    private final List<T> objects = new ArrayList<>();

    public ObjectManager(MilkaPlugin plugin) {
        super(plugin);
    }

    /**
     * Add object to manager's list
     *
     * @param object object to add
     */
    public void add(T object) {
        objects.add(object);
    }

    /**
     * Remove object from manager's list
     *
     * @param object object to remove
     */
    public void remove(T object) {
        objects.remove(object);
    }

    /**
     * Check if object is stored in manger
     *
     * @param object targeted object
     * @return true if it's contained in manager's list
     */
    public boolean contains(T object) {
        return objects.contains(object);
    }
}
