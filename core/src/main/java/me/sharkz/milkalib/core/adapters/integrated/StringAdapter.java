package me.sharkz.milkalib.core.adapters.integrated;

import me.sharkz.milkalib.core.adapters.MilkaAdapter;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class StringAdapter implements MilkaAdapter<String> {

    @Override
    public Optional<String> parse(String data) {
        return Optional.of(data);
    }

    @Override
    public Collection<String> getSuggestions() {
        return Collections.emptyList();
    }
}
