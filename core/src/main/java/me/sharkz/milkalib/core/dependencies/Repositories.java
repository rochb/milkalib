package me.sharkz.milkalib.core.dependencies;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
@Getter
public enum Repositories {

    MAVEN_CENTRAL("https://repo1.maven.org/maven2/"),
    SONATYPE("https://oss.sonatype.org/content/repositories/releases/");

    private final String url;
}
