package me.sharkz.milkalib.core.storage;

import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.storage.implementations.sql.ConnectionFactory;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.sql.SQLException;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class StorageFactory {

    private final MilkaPlugin plugin;
    private final StorageType type;
    private final StorageCredentials credentials;
    private final ConnectionFactory factory;

    public StorageFactory(MilkaPlugin plugin, StorageType type, StorageCredentials credentials) {
        this.plugin = plugin;
        this.type = type;
        this.credentials = credentials;
        this.factory = getConnectionFactory();
    }

    private ConnectionFactory getConnectionFactory() {
        ConnectionFactory factory = null;
        Class<? extends ConnectionFactory> factoryClass = type.getFactoryClass();
        try {
            if (type.isFlatFile())
                factory = factoryClass.getConstructor(Path.class).newInstance(new File(plugin.getDataFolder(), plugin.getName().toLowerCase()).toPath());
            else if (type.isRemote())
                factory = factoryClass.getConstructor(StorageCredentials.class).newInstance(credentials);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return factory;
    }

    public MilkaStorage build() {
        try {
            this.factory.init();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return new MilkaStorage(plugin, factory);
    }
}
