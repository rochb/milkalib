package me.sharkz.milkalib.core.adapters.integrated;

import me.sharkz.milkalib.core.adapters.MilkaAdapter;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BooleanAdapter implements MilkaAdapter<Boolean> {

    @Override
    public Optional<Boolean> parse(String data) {
        try {
            return Optional.of(Boolean.parseBoolean(data));
        } catch (Exception ignored) {
            return Optional.empty();
        }
    }

    @Override
    public Collection<String> getSuggestions() {
        return Arrays.asList("true", "false");
    }
}
