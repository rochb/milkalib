package me.sharkz.milkalib.core.dependencies.classloader;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum LoaderType {
    ISOLATED,
    REFLECTION;
}
