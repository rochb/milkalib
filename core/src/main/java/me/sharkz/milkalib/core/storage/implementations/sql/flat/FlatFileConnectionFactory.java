package me.sharkz.milkalib.core.storage.implementations.sql.flat;

import com.zaxxer.hikari.HikariConfig;
import lombok.RequiredArgsConstructor;
import me.sharkz.milkalib.core.storage.StorageCredentials;
import me.sharkz.milkalib.core.storage.implementations.sql.ConnectionFactory;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public abstract class FlatFileConnectionFactory implements ConnectionFactory {

    private final Path file;
    protected NonClosableConnection connection;

    protected abstract Connection createConnection(Path file) throws SQLException;

    @Override
    public void init() throws SQLException {
        this.connection = new NonClosableConnection(createConnection(getWriteFile()));
    }

    @Override
    public void shutdown() throws SQLException {
        if (Objects.nonNull(connection)) connection.shutdown();
    }

    @Override
    public Connection getConnection() throws SQLException {
        NonClosableConnection connection = this.connection;
        if (Objects.isNull(connection) || connection.isClosed()) {
            connection = new NonClosableConnection(createConnection(file));
            this.connection = connection;
        }
        return connection;
    }

    @Override
    public void postInitialize() {

    }

    @Override
    public void configure(HikariConfig config, StorageCredentials credentials) {

    }

    protected Path getWriteFile() {
        return file;
    }
}
