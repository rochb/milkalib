package me.sharkz.milkalib.core.storage;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.sharkz.milkalib.core.storage.implementations.sql.ConnectionFactory;
import me.sharkz.milkalib.core.storage.implementations.sql.flat.FlatFileConnectionFactory;
import me.sharkz.milkalib.core.storage.implementations.sql.flat.H2ConnectionFactory;
import me.sharkz.milkalib.core.storage.implementations.sql.flat.SqliteConnectionFactory;
import me.sharkz.milkalib.core.storage.implementations.sql.remote.HikariConnectionFactory;
import me.sharkz.milkalib.core.storage.implementations.sql.remote.MariaDBConnectionFactory;
import me.sharkz.milkalib.core.storage.implementations.sql.remote.MySQLConnectionFactory;
import me.sharkz.milkalib.core.storage.implementations.sql.remote.PostgreConnectionFactory;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@RequiredArgsConstructor
public enum StorageType {
    // FLAT
    SQLITE(SqliteConnectionFactory.class),
    H2(H2ConnectionFactory.class),

    // REMOTE
    MYSQL(MySQLConnectionFactory.class),
    MARIADB(MariaDBConnectionFactory.class),
    POSTGRE(PostgreConnectionFactory.class);

    private final Class<? extends ConnectionFactory> factoryClass;

    public boolean isFlatFile() {
        return Objects.nonNull(factoryClass.getSuperclass()) && factoryClass.getSuperclass().equals(FlatFileConnectionFactory.class);
    }

    public boolean isRemote() {
        return Objects.nonNull(factoryClass.getSuperclass()) && factoryClass.getSuperclass().equals(HikariConnectionFactory.class);
    }

}
