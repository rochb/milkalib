package me.sharkz.milkalib.core.storage;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@AllArgsConstructor
@Getter
public class StorageCredentials {

    private final String address;
    private final String database;
    private final int port;
    private final String username;
    private final String password;
    private final int maxPoolSize;
    private final int minIdleConnections;
    private final int maxLifetime;
    private final int keepAliveTime;
    private final int connectionTimeout;
    private final Map<String, String> properties;

}
