package me.sharkz.milkalib.core.storage.implementations.sql.remote;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import me.sharkz.milkalib.core.storage.StorageCredentials;
import me.sharkz.milkalib.core.storage.implementations.sql.ConnectionFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public abstract class HikariConnectionFactory implements ConnectionFactory {

    private final StorageCredentials credentials;
    private HikariDataSource hikari;

    @Override
    public void init() {
        HikariConfig config = new HikariConfig();

        config.setPoolName("milkalib-hikari");

        configure(config, credentials);

        Map<String, String> properties = new HashMap<>(credentials.getProperties());

        overrideProperties(properties);

        setProperties(config, properties);

        config.setMaximumPoolSize(credentials.getMaxPoolSize());
        config.setMinimumIdle(credentials.getMinIdleConnections());
        config.setMaxLifetime(credentials.getMaxLifetime());
        config.setKeepaliveTime(credentials.getKeepAliveTime());
        config.setConnectionTimeout(credentials.getConnectionTimeout());
        config.setAutoCommit(true);

        config.setInitializationFailTimeout(-1);

        this.hikari = new HikariDataSource(config);

        this.postInitialize();
    }

    @Override
    public void shutdown() {
        if (Objects.nonNull(hikari)) hikari.close();
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (Objects.isNull(hikari))
            throw new SQLException("Unable to get a connection from the pool. (hikari is null)");
        Connection connection = hikari.getConnection();
        if (Objects.isNull(connection))
            throw new SQLException("Unable to get a connection from the pool. (getConnection returned null)");
        return connection;
    }

    @Override
    public void postInitialize() {

    }

    /**
     * Allows the connection factory instance to override certain properties before they are set.
     *
     * @param properties the current properties
     */
    protected void overrideProperties(Map<String, String> properties) {
        // https://github.com/brettwooldridge/HikariCP/wiki/Rapid-Recovery
        properties.putIfAbsent("socketTimeout", String.valueOf(TimeUnit.SECONDS.toMillis(30)));
    }

    /**
     * Sets the given connection properties onto the config.
     *
     * @param config     the hikari config
     * @param properties the properties
     */
    protected void setProperties(HikariConfig config, Map<String, String> properties) {
        for (Map.Entry<String, String> property : properties.entrySet())
            config.addDataSourceProperty(property.getKey(), property.getValue());
    }

}
