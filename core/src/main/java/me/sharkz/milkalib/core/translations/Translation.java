package me.sharkz.milkalib.core.translations;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@RequiredArgsConstructor
public class Translation {

    private final TranslationPack pack;
    private final String id;
    private final List<String> value;

    public List<String> translate() {
        return this.pack.getManager().translate(this.id, this.value);
    }
}
