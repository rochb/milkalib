package me.sharkz.milkalib.core.hooks;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface PluginHooker {

    /**
     * Get required plugin name
     *
     * @return plugin name
     */
    String getRequiredPlugin();

    /**
     * Method called on hook success
     */
    void onHookSuccess();

    /**
     * Method called on hook fail
     */
    void onHookFail();

    /**
     * Check if plugin has been hooked successfully
     *
     * @return true if it's the case
     */
    boolean isHooked();

    /**
     * Disable plugin is hooked plugin is not present
     *
     * @return true if it's the case
     */
    boolean disablePluginIfNotFound();
}
