package me.sharkz.milkalib.core.adapters.integrated;

import me.sharkz.milkalib.core.adapters.MilkaAdapter;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class IntegerAdapter implements MilkaAdapter<Integer> {

    @Override
    public Optional<Integer> parse(String data) {
        try {
            return Optional.of(Integer.parseInt(data));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public Collection<String> getSuggestions() {
        return Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100");
    }
}
