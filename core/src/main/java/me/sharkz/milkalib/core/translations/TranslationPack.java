package me.sharkz.milkalib.core.translations;

import java.util.List;
import java.util.Locale;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface TranslationPack {

    /**
     * Gets the language of the file
     *
     * @return Language locale of the file
     */
    Locale getLanguage();

    /**
     * Gets a single translation
     *
     * @return Current Translation
     */
    Translation get();

    /**
     * Gets all the available translations
     *
     * @return Available translations
     */
    List<Translation> translations();

    /**
     * Sets the translation manager
     *
     * @param manager Translation Manager
     */
    void setManager(TranslationManager manager);

    /**
     * Gets the translation manager
     *
     * @return Translation manager
     */
    TranslationManager getManager();

    /**
     * This will return the language display name in his own language
     * <p>
     * If the language is es_ES it will return "Español",
     * if the language is en_US it will return "English"
     *
     * @return The name of the language
     */
    default String getDisplayLanguage() {
        return this.getLanguage().getDisplayLanguage(this.getLanguage());
    }
}
