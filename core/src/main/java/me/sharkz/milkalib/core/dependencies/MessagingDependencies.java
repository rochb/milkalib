package me.sharkz.milkalib.core.dependencies;

import lombok.Getter;
import me.sharkz.milkalib.core.dependencies.classloader.LoaderType;

import java.util.Arrays;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public enum MessagingDependencies {
    REDIS_DRIVER(
            "redis.clients",
            "jedis",
            "3.6.0",
            "xx4FQqEm3e+jo1wgjzt7azBbaNYNHmBNfZJjzXcHCvc=",
            LoaderType.ISOLATED
    ),
    RABBITMQ_DRIVER(
            "com.rabbitmq",
            "amqp-client",
            "5.12.0",
            "CxliwVWAnPKi5BwxCu1S1SGzx5fbhTk5JCKdBS27P2c=",
            LoaderType.ISOLATED
    );

    private final Dependency dependency;

    MessagingDependencies(String groupId, String artifactId, String version, String checksum, LoaderType loaderType) {
        this.dependency = new Dependency(name(), groupId, artifactId, version, checksum, loaderType);
    }

    public static Dependency[] get() {
        return Arrays.stream(values()).map(MessagingDependencies::getDependency).toArray(Dependency[]::new);
    }
}
