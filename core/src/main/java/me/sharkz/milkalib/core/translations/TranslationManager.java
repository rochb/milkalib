package me.sharkz.milkalib.core.translations;

import me.sharkz.milkalib.core.MilkaPlugin;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import sun.misc.Unsafe;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class TranslationManager {

    private Map<Locale, LinkedHashMap<String, List<String>>> translations;
    private final List<Class<? extends TranslationPack>> registered;
    protected MilkaPlugin plugin;

    private final Yaml yaml;

    public TranslationManager(MilkaPlugin plugin) {
        this.plugin = plugin;
        this.translations = new LinkedHashMap<>();
        this.registered = new LinkedList<>();

        DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setIndicatorIndent(2);
        options.setIndentWithIndicator(true);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);
        options.setAllowUnicode(true);
        this.yaml = new Yaml(options);
    }

    public boolean isRegistered(Class<? extends TranslationPack> translationClass) {
        return registered.contains(translationClass);
    }

    /**
     * Read YML File with natives methods.
     *
     * @param file translation file to read.
     * @return map of string
     */
    protected Map<String, List<String>> readYMLFile(File file) throws FileNotFoundException {
        return ((Map<String, Object>) yaml.load(new FileInputStream(file)))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> {
                    if (e.getValue() instanceof List) return (List<String>) e.getValue();
                    return Collections.singletonList((String) e.getValue());
                }));
    }

    protected void saveMapToFile(File file, Map<String, List<String>> objects) throws IOException {
        Map<String, Object> data = objects
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> {
                    if (e.getValue().size() > 1) return e.getValue();
                    return e.getValue().get(0);
                }));
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
        yaml.dump(data, writer);
        writer.close();
    }

    /**
     * Register a new default translation
     * (The generated file will be replaced after every start)
     *
     * @param translationClass {@link TranslationPack} class to register
     */
    public void register(Class<? extends TranslationPack> translationClass) {
        if (isRegistered(translationClass)) return;
        plugin.initTranslationsFolder();

        try {
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            Unsafe unsafe = (Unsafe) theUnsafe.get(null);
            TranslationPack pack = ((TranslationPack) unsafe.allocateInstance(translationClass));

            List<Translation> translations = pack.translations()
                    .stream()
                    .peek(translation -> translation.getPack().setManager(this))
                    .collect(Collectors.toList());
            LinkedHashMap<String, List<String>> translationsMap = new LinkedHashMap<>();

            File file = new File(plugin.getTranslationsFolder(), pack.getLanguage().toString() + ".yml");
            if (file.exists()) {
                translationsMap.putAll(readYMLFile(file));
                file.delete();
            }
            translations.forEach(t -> translationsMap.putIfAbsent(formatKey(t.getId()), t.getValue()));
            file.createNewFile();

            saveMapToFile(file, translationsMap);

            List<String> lines = new ArrayList<>(Arrays.asList("# " + plugin.getName(),
                    "#",
                    "# Developed by Sharkz",
                    "# www.roch-blondiaux.com",
                    "#",
                    "# Default " + pack.getDisplayLanguage() + " translation",
                    "# If you want to edit the translations copy this file and rename it",
                    "# The file name format is 'language_COUNTRY.lang'",
                    "#",
                    "# If you're translating this file, remember to not translate placeholders nor keys!"));

            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
                String line;
                while ((line = br.readLine()) != null)
                    lines.add(line);
            }

            PrintWriter pw = new PrintWriter(new FileWriter(file));
            lines.forEach(pw::println);
            pw.close();

            registered.add(translationClass);

            reloadTranslations();
        } catch (NoSuchFieldException | IllegalAccessException | InstantiationException | IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Clear the caches and load all the available translations
     */
    public void reloadTranslations() {
        if (Objects.isNull(translations)) translations = new LinkedHashMap<>();
        plugin.initTranslationsFolder();

        translations.clear();
        File[] translationsFiles = plugin.getTranslationsFolder().listFiles();
        if (Objects.isNull(translationsFiles) || translationsFiles.length == 0) return;
        Arrays.stream(translationsFiles)
                .filter(file -> file.getName().endsWith(".yml") && file.getName().contains("_"))
                .forEach(file -> {
                    String name = file.getName().replace(".yml", "");
                    String lang = name.split("_")[0];
                    String country = name.split("_")[1];
                    Locale locale = new Locale(lang, country);
                    LinkedHashMap<String, List<String>> translations = this.translations.getOrDefault(locale, new LinkedHashMap<>());
                    try {
                        readYMLFile(file).forEach(translations::put);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    this.translations.put(locale, translations);
                });
    }

    /**
     * Translates a string into current language
     *
     * @param id  Identifier of the translation
     * @param def Default value in case that the id doesnt exists
     * @return Translated identifier
     */
    public List<String> translate(String id, List<String> def) {
        String currentLanguage = plugin.getLanguage();
        Locale locale = new Locale(currentLanguage.split("_")[0], currentLanguage.split("_")[1]);
        if (translations.containsKey(locale)) return translations.get(locale).getOrDefault(formatKey(id), def);
        if (locale.equals(new Locale("en", "US"))) return def;
        return translations.get(new Locale("en", "US")).getOrDefault(formatKey(id), def);
    }

    public List<String> translate(String id, String def) {
        return translate(id, Collections.singletonList(def));
    }

    /**
     * Gets the available translations
     *
     * @return the available translations
     */
    public Locale[] getTranslations() {
        return translations.keySet().toArray(new Locale[]{});
    }

    private String formatKey(String key) {
        return key.toLowerCase().replace("_", "-");
    }
}
