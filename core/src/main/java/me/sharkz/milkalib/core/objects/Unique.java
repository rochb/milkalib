package me.sharkz.milkalib.core.objects;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface Unique {

    /**
     * Get object's unique id
     *
     * @return unique id
     */
    UUID getUniqueId();
}
