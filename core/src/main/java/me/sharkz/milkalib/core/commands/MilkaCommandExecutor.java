package me.sharkz.milkalib.core.commands;

import lombok.AccessLevel;
import lombok.Getter;
import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.adapters.AdaptersManager;
import me.sharkz.milkalib.core.utils.MilkaUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter(AccessLevel.PROTECTED)
public abstract class MilkaCommandExecutor extends MilkaUtils {

    private final CommandsManager<?> manager;
    private final AdaptersManager adaptersManager;

    public MilkaCommandExecutor(MilkaPlugin plugin, CommandsManager<?> manager, AdaptersManager adaptersManager) {
        super(plugin);
        this.manager = manager;
        this.adaptersManager = adaptersManager;
    }

    public Object[] parseArgs(MilkaCommand command, int start, String[] args) {
        if (args.length <= start) return new Object[]{};
        Object[] a = new Object[args.length - start];
        int index = 0;
        for (int i = start; i < args.length; i++) {
            Optional<Class<?>> argumentType = Optional.empty();
            try {
                argumentType = command.getArgumentType(index);
            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
                a[index] = null;
            }
            int finalI = i;
            a[index] = argumentType.flatMap(adaptersManager::getAdapter)
                    .flatMap(adapter -> adapter.parse(args[finalI]))
                    .orElse(null);
            index++;
        }
        return a;
    }

    public CommandResult areArgsValid(MilkaCommand command, Object[] args, int argIndex) {
        if (args.length < command.getRequiredArguments().size())
            return CommandResult.MISSING_ARGUMENT;
        else if (args.length > command.getRequiredArguments().size() + command.getOptionalArguments().size())
            return CommandResult.INVALID_SYNTAX;

        for (int i = argIndex; i < args.length; i++) {
            Optional<Class<?>> arg;
            try {
                arg = command.getArgumentType(i);
            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
                return CommandResult.INVALID_ARGUMENT_TYPE;
            }
            if (Objects.isNull(args[i]) || Objects.isNull(arg)) return CommandResult.INVALID_SYNTAX;

            Class<?> adapterType = arg.flatMap(adaptersManager::getAdapter)
                    .map(adaptersManager::getGenericClass)
                    .orElse(null);
            if (Objects.isNull(adapterType) || !adapterType.equals(arg.get()))
                return CommandResult.INVALID_ARGUMENT_TYPE;
        }
        return CommandResult.SUCCESS;
    }

    public List<String> getTabSuggestions(MilkaCommand cmd, int index, String[] args, PermissionChecker checker) {
        int length = args.length - index;
        List<String> a = new ArrayList<>();

        try {
            cmd.getArgumentType(length - 1).flatMap(adaptersManager::getAdapter).ifPresent(milkaAdapter -> a.addAll(milkaAdapter.getSuggestions()));
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }

        if (length == 1 && cmd.getSubCommands().size() > 0)
            a.addAll(cmd.getSubCommands()
                    .stream()
                    .filter(milkaCommand -> (Objects.isNull(milkaCommand.getPermission()) || milkaCommand.getPermission().isEmpty()) || checker.hasPermission(milkaCommand.getPermission()))
                    .map(MilkaCommand::getKey)
                    .collect(Collectors.toList()));
        else if (length >= 1)
            cmd.getSubCommands().stream()
                    .filter(milkaCommand -> (Objects.isNull(milkaCommand.getPermission()) || milkaCommand.getPermission().isEmpty()) || checker.hasPermission(milkaCommand.getPermission()))
                    .filter(command -> command.getKey().equalsIgnoreCase(args[index]) || command.getAliases().stream().anyMatch(s -> s.equalsIgnoreCase(args[index])))
                    .findFirst()
                    .ifPresent(command -> a.addAll(getTabSuggestions(command, index + 1, args, checker)));
        return a;
    }

    protected List<String> tabLimit(final List<String> list, final String start) {
        return list.stream()
                .filter(Objects::nonNull)
                .filter(s -> s.toLowerCase().startsWith(start.toLowerCase()))
                .collect(Collectors.toList());
    }
}
