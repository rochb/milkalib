package me.sharkz.milkalib.core.dependencies.classloader;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */

import java.nio.file.Path;

/**
 * Represents the plugins classloader
 */
public interface PluginClassLoader {

    void loadJar(Path file);

}