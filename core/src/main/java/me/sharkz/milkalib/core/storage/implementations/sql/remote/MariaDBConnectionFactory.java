package me.sharkz.milkalib.core.storage.implementations.sql.remote;

import com.zaxxer.hikari.HikariConfig;
import me.sharkz.milkalib.core.storage.StorageCredentials;
import me.sharkz.milkalib.core.storage.StorageType;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class MariaDBConnectionFactory extends HikariConnectionFactory {

    public MariaDBConnectionFactory(StorageCredentials credentials) {
        super(credentials);
    }

    @Override
    public StorageType getType() {
        return StorageType.MARIADB;
    }

    @Override
    public void configure(HikariConfig config, StorageCredentials credentials) {
        config.setDataSourceClassName("org.mariadb.jdbc.MariaDbDataSource");
        config.addDataSourceProperty("serverName", credentials.getAddress());
        config.addDataSourceProperty("port", credentials.getPort());
        config.addDataSourceProperty("databaseName", credentials.getDatabase());
        config.setUsername(credentials.getUsername());
        config.setPassword(credentials.getPassword());
    }

    @Override
    protected void setProperties(HikariConfig config, Map<String, String> properties) {
        String propertiesString = properties.entrySet().stream()
                .map(e -> e.getKey() + "=" + e.getValue())
                .collect(Collectors.joining(";"));

        config.addDataSourceProperty("properties", propertiesString);
    }
}
