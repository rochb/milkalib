package me.sharkz.milkalib.core.commands;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.exceptions.CommandRegistrationException;
import me.sharkz.milkalib.core.exceptions.CommandUnregistrationException;
import me.sharkz.milkalib.core.translations.integrated.CommandsEn;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter(AccessLevel.PROTECTED)
public abstract class CommandsManager<T extends MilkaCommand> {

    private final MilkaPlugin plugin;

    @Setter(AccessLevel.PROTECTED)
    private MilkaCommandExecutor executor;

    public CommandsManager(MilkaPlugin plugin) {
        this.plugin = plugin;
        this.plugin.getTranslationManager().register(CommandsEn.class);
    }

    private final List<T> commands = new ArrayList<>();

    /**
     * Know if a command is already registered
     *
     * @param command targeted command
     * @return true if it is registered
     */
    public boolean isRegistered(T command) {
        return commands.contains(command);
    }

    /**
     * Get a command by it's key
     *
     * @param key targeted key
     * @return optional of milka command extended class
     */
    public Optional<T> getKyKey(String key) {
        return commands.stream().filter(t -> t.getKey().equalsIgnoreCase(key)).findFirst();
    }

    /**
     * Get a command by it's key or it's aliases
     *
     * @param keyOrAliases targeted key or aliases
     * @return optional of milka command extended class
     */
    public Optional<T> getByKeyOrAliases(String keyOrAliases) {
        return commands.stream().filter(t -> t.getKey().equalsIgnoreCase(keyOrAliases) || t.getAliases().stream().anyMatch(s -> s.equalsIgnoreCase(keyOrAliases))).findFirst();
    }

    /**
     * Know if command's key has already been registered
     *
     * @param command targeted command
     * @return true if command's key is already registered
     */
    protected boolean isKeyRegistered(T command) {
        return getByKeyOrAliases(command.getKey()).isPresent();
    }

    /**
     * Know if any command's aliases has already been registered
     *
     * @param command targeted command
     * @return true if any command's aliases is already registered
     */
    protected boolean areAliasesRegistered(T command) {
        return commands.stream().anyMatch(t -> t.getAliases().stream().anyMatch(s -> command.getAliases().stream().anyMatch(s1 -> s1.equalsIgnoreCase(s))));
    }

    /**
     * Register command by it's class
     * <p>
     * {@link this.regisyer(T)}
     *
     * @param commandClass         command's class
     * @param constructorArguments command's constructor arguments
     */
    public void register(Class<? extends T> commandClass, Object... constructorArguments) {
        try {
            Constructor<? extends T> constructor = commandClass.getDeclaredConstructor(Arrays.stream(constructorArguments).map(Object::getClass).collect(Collectors.toList()).toArray(new Class[]{}));
            T command = constructor.newInstance(constructorArguments);
            register(command);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | CommandRegistrationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Register command
     *
     * @param command targeted command
     * @throws CommandRegistrationException thrown if module can't register targeted command
     */
    public void register(T command) throws CommandRegistrationException {
        if (isKeyRegistered(command) || areAliasesRegistered(command))
            throw new CommandRegistrationException("Plugin " + plugin.getName() + " is trying to register a command with already registered key or aliases!");
        if (registerCommand(command)) commands.add(command);
    }

    /**
     * Unregister a command
     *
     * @param command targeted command
     * @throws CommandUnregistrationException thrown if module can't unregister targeted command
     */
    public void unregister(T command) throws CommandUnregistrationException {
        if (!isRegistered(command))
            throw new CommandUnregistrationException("Plugin " + plugin.getName() + " is trying to unregister an unknown command!");
        if (unregisterCommand(command)) commands.remove(command);
    }

    /**
     * Call native module to register a command
     *
     * @param command targeted command
     * @return true if command has been registered by module
     */
    protected abstract boolean registerCommand(T command) throws CommandRegistrationException;

    /**
     * Call native module to unregister a command
     *
     * @param command targeted command
     * @return true if command has been unregistered by module
     */
    protected abstract boolean unregisterCommand(T command);
}
