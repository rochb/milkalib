package me.sharkz.milkalib.core.utils;

import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.configurations.wrapper.YmlFileWrapper;
import me.sharkz.milkalib.core.scheduler.SchedulerTask;
import me.sharkz.milkalib.core.translations.TranslationPack;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class MilkaUtils {

    private final MilkaPlugin plugin;

    public MilkaUtils(MilkaPlugin plugin) {
        this.plugin = plugin;
    }

    public MilkaPlugin getPlugin() {
        return plugin;
    }

    /**
     * Get nearest multiple of a number
     *
     * @param i      number to format
     * @param number nearest multiple of
     * @return nearest multiple
     */
    protected int getNearestMultiple(int i, int number) {
        return ((i + (number - 1)) / number) * number;
    }

    /**
     * Get placeholders map from objects array
     *
     * @param objects placeholders as object arrays
     * @return placeholders as string map
     */
    protected Map<String, String> getPlaceholders(Object... objects) {
        Map<String, String> placeholders = new LinkedHashMap<>();
        for (int i = 0; i < objects.length; i++) {
            if (i + 1 >= objects.length || Objects.isNull(objects[i + 1])) continue;
            placeholders.put(objects[i].toString(), objects[i + 1].toString());
        }
        return placeholders;
    }

    /**
     * Format string with placeholders
     *
     * @param text         string to format
     * @param placeholders placeholders map
     * @return formatted text
     */
    protected String format(String text, Map<String, String> placeholders) {
        for (String key : placeholders.keySet())
            text = text.replace('%' + key + '%', placeholders.get(key));
        return text;
    }

    /**
     * Format string collection with placeholders
     * <p>
     * {@link this.format(String, Map)}
     *
     * @param a            string collection to format
     * @param placeholders placeholders map
     * @return formatted collection
     */
    protected Collection<String> format(Collection<String> a, Map<String, String> placeholders) {
        return a.stream().map(s -> format(s, placeholders)).collect(Collectors.toList());
    }

    /**
     * Format string array with placeholders
     * <p>
     * {@link this.format(String, Map)}
     *
     * @param a            string array to format
     * @param placeholders placeholders map
     * @return formatted string array
     */
    protected String[] format(String[] a, Map<String, String> placeholders) {
        String[] b = new String[a.length];
        for (int i = 0; i < a.length; i++)
            b[i] = format(a[i], placeholders);
        return b;
    }

    /**
     * Format string with placeholders
     * <p>
     * {@link this.getPlaceholders()}
     * {@link this.format(String, Map)}
     *
     * @param text         string to format
     * @param placeholders array of placeholders (converted to map)
     * @return formatted placeholders
     */
    protected String format(String text, Object... placeholders) {
        return format(text, getPlaceholders(placeholders));
    }

    /**
     * Format string collection with placeholders array
     * <p>
     * {@link this.format(Collection, Map)}
     * {@link this.getPlaceholders(Object...)}
     *
     * @param a            string collection to format
     * @param placeholders placeholders array
     * @return formatted string collection
     */
    protected Collection<String> format(Collection<String> a, Object... placeholders) {
        return format(a, getPlaceholders(placeholders));
    }

    /**
     * Format string array with placeholders array
     * <p>
     * {@link this.format(String[], Map)}
     * {@link this.getPlaceholders(Object...)}
     *
     * @param a            string array to format
     * @param placeholders placeholders array
     * @return formatted string array
     */
    protected String[] format(String[] a, Object... placeholders) {
        return format(a, getPlaceholders(placeholders));
    }

    /**
     * Capitalize first letter of string
     *
     * @param a string to capitalise
     * @return formatted string
     */
    protected String capitalize(String a) {
        return Character.toUpperCase(a.charAt(0)) + a.substring(1);
    }

    /**
     * Capitalize first letter of each word
     *
     * @param a string to format
     * @return formatted string
     */
    protected String capitalizeAllWords(String a) {
        StringBuilder builder = new StringBuilder();
        String[] b = a.split(" ");
        for (int i = 0; i < b.length; i++) {
            builder.append(capitalize(b[i]));
            if (i < b.length - 1) builder.append(" ");
        }
        return builder.toString();
    }

    /**
     * Paginate list
     *
     * @param list     to manipulate
     * @param pageSize objects per page
     * @param page     current page index
     * @param <T>      object
     * @return paginated list
     */
    protected <T> List<T> paginate(List<T> list, int pageSize, int page) {
        int from = Math.max(0, page * pageSize);
        int to = Math.min(list.size(), (page + 1) * pageSize);
        if (from >= to) return list;
        return list.subList(from, to);
    }

    /**
     * Get max page from collection & page size
     *
     * @param objects list of objects
     * @param a       page size
     * @return max pages count
     */
    protected int getMaxPage(Collection<?> objects, int a) {
        return (objects.size() / a) + 1;
    }

    /**
     * Translate minecraft color codes
     *
     * @param a string list to color
     * @return colored string
     */
    protected String color(String a) {
        return MilkaColor.translate(a);
    }

    /**
     * Translate minecraft color codes
     *
     * @param a string collection to color
     * @return colored string collection
     */
    protected Collection<String> color(Collection<String> a) {
        return a.stream().map(this::color).collect(Collectors.toList());
    }

    /**
     * Translate minecraft color codes
     *
     * @param a sting array to color
     * @return colored string array
     */
    protected String[] color(String... a) {
        return color(Arrays.asList(a)).toArray(new String[]{});
    }

    /**
     * Get YmlFileWrapper by it's class
     *
     * @param wrapperClass wrapper's class
     * @param <T>          wrapper's class type
     * @return wrapper instance
     */
    protected <T extends YmlFileWrapper> Optional<T> getConfig(Class<? extends YmlFileWrapper> wrapperClass) {
        return (Optional<T>) plugin.getConfigurationManager().getByClass(wrapperClass);
    }

    /**
     * Translate translation file entry
     *
     * @param translation translation entry
     * @param <T>         translation class
     * @return translated string
     */
    protected <T extends TranslationPack> List<String> translate(T translation) {
        return getPlugin().getTranslationManager().translate(translation.get().getId(), translation.get().getValue());
    }

    /**
     * Color and translate message
     *
     * @param translation translation entry
     * @param <T>         translation class
     * @return colored & translated messages
     */
    protected <T extends TranslationPack> Collection<String> color(T translation) {
        return color(translate(translation));
    }

    /**
     * Translate and format message
     *
     * @param translation  translation entry
     * @param placeholders placeholders
     * @param <T>          translation class
     * @return translated & formatted messages
     */
    protected <T extends TranslationPack> Collection<String> format(T translation, Object... placeholders) {
        return format(translate(translation), placeholders);
    }

    /**
     * Translate and format message
     *
     * @param translation  translation entry
     * @param placeholders placeholders map
     * @param <T>          translation class
     * @return translated & formatted messages
     */
    protected <T extends TranslationPack> Collection<String> format(T translation, Map<String, String> placeholders) {
        return format(translate(translation), placeholders);
    }

    /**
     * Translate, color and format message
     *
     * @param translation  translation entry
     * @param placeholders placeholders map
     * @param <T>          translation class
     * @return translated, colored & formatted messages
     */
    protected <T extends TranslationPack> Collection<String> colorAndFormat(T translation, Map<String, String> placeholders) {
        return color(format(translation, placeholders));
    }

    /**
     * Translate, color and format message
     *
     * @param translation  translation entry
     * @param placeholders placeholders
     * @param <T>          translation class
     * @return translated, colored & formatted messages
     */
    protected <T extends TranslationPack> Collection<String> colorAndFormat(T translation, Object... placeholders) {
        return color(format(translation, placeholders));
    }

    /**
     * Check if a string is null or empty
     *
     * @param a string to check
     * @return true if it's the case
     */
    protected boolean isNullOrEmpty(String a) {
        return Objects.isNull(a) || a.isEmpty();
    }

    /**
     * Execute runnable synchronously
     *
     * @param runnable to execute
     */
    protected void runSync(Runnable runnable) {
        plugin.getSchedulerAdapter().executeSync(runnable);
    }

    /**
     * Execute runnable asynchronously
     *
     * @param runnable to execute
     */
    protected void runAsync(Runnable runnable) {
        plugin.getSchedulerAdapter().executeAsync(runnable);
    }

    /**
     * Execute runnable asynchronously with delay
     *
     * @param runnable to execute
     * @param delay    time before executing
     * @param unit     time unit
     * @return the resultant task instance
     */
    protected SchedulerTask runAsyncLater(Runnable runnable, long delay, TimeUnit unit) {
        return plugin.getSchedulerAdapter().asyncLater(runnable, delay, unit);
    }

    /**
     * Execute runnable asynchronously at fixed rate
     *
     * @param runnable to execute
     * @param period   time between each execution
     * @param unit     time unit
     * @return the resultant task instance
     */
    protected SchedulerTask runAsyncRepeating(Runnable runnable, long period, TimeUnit unit) {
        return plugin.getSchedulerAdapter().asyncRepeating(runnable, period, unit);
    }

    /**
     * Execute runnable asynchronously at fixed rate with delay
     *
     * @param runnable to execute
     * @param delay    time before first execution
     * @param period   time between each execution
     * @param unit     time unit
     */
    protected void runAsyncRepeatingDelayed(Runnable runnable, long delay, long period, TimeUnit unit) {
        runAsyncLater(() -> runAsyncRepeating(runnable, period, unit), delay, unit);
    }

    protected void info(String message) {
        plugin.getMilkaLogger().info(message);
    }

    protected void success(String message) {
        plugin.getMilkaLogger().success(message);
    }

    protected void warn(String message) {
        plugin.getMilkaLogger().warn(message);
    }

    protected void error(String message) {
        plugin.getMilkaLogger().error(message);
    }

    protected void debug(String message) {
        plugin.getMilkaLogger().debug(message);
    }

}
