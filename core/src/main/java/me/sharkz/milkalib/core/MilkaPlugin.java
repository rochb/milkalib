package me.sharkz.milkalib.core;

import me.sharkz.milkalib.core.adapters.AdaptersManager;
import me.sharkz.milkalib.core.adapters.MilkaAdapter;
import me.sharkz.milkalib.core.commands.CommandsManager;
import me.sharkz.milkalib.core.configurations.MilkaConfigurationManager;
import me.sharkz.milkalib.core.configurations.wrapper.YmlFileWrapper;
import me.sharkz.milkalib.core.dependencies.DependencyManager;
import me.sharkz.milkalib.core.logger.MilkaLogger;
import me.sharkz.milkalib.core.scheduler.SchedulerAdapter;
import me.sharkz.milkalib.core.translations.TranslationManager;

import java.io.File;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface MilkaPlugin {

    /**
     * Get plugin's name
     *
     * @return plugin's name
     */
    String getName();

    /**
     * Get plugin's language
     *
     * @return plugin's language
     */
    String getLanguage();

    /**
     * Get plugin's data folder
     *
     * @return plugin's data folder
     */
    File getDataFolder();

    /**
     * Get plugin's translations folder
     *
     * @return plugin's translations folder
     */
    default File getTranslationsFolder() {
        return new File(getDataFolder(), "lang");
    }

    /**
     * Get plugin's dependencies folder
     * @return plugin's dependencies folder
     */
    default File getDependenciesFolder() {
        return new File(getDataFolder(), "libs");
    }

    /**
     * Mkdir data folder if it not exists
     */
    default void initDataFolder() {
        if (Objects.nonNull(getDataFolder()) && !getDataFolder().exists()) getDataFolder().mkdir();
    }

    default void initTranslationsFolder() {
        if (Objects.nonNull(getTranslationsFolder()) && !getTranslationsFolder().exists())
            getTranslationsFolder().mkdir();
    }

    default void initDependenciesFolder() {
        if (Objects.nonNull(getDependenciesFolder()) && !getDependenciesFolder().exists())
            getDependenciesFolder().mkdir();
    }

    /**
     * Method called on plugin reload
     */
    void onReload();

    /**
     * Get plugin's adapters manager
     *
     * @return adapters manager instance
     */
    AdaptersManager getAdaptersManager();

    /**
     * Register single type adapter
     *
     * @param adapter type adapter
     */
    default void registerAdapter(Class<? extends MilkaAdapter<?>> adapter) {
        getAdaptersManager().register(adapter);
    }

    /**
     * Register single type adapter with constructor arguments
     *
     * @param adapter type adapter
     * @param objects constructor's arguments
     */
    default void registerAdapter(Class<? extends MilkaAdapter<?>> adapter, Object... objects) {
        getAdaptersManager().register(adapter, objects);
    }

    /**
     * Register multiple type adapters
     *
     * @param adapters type adapters array
     */
    default void registerAdapters(Class<? extends MilkaAdapter<?>>... adapters) {
        getAdaptersManager().register(adapters);
    }

    /**
     * Get plugin's commands manager
     *
     * @return command manager
     */
    CommandsManager<?> getCommandsManager();

    /**
     * Get plugin's configuration manager
     *
     * @return configuration manager
     */
    MilkaConfigurationManager<?> getConfigurationManager();

    /**
     * Get plugin's translation manager
     *
     * @return translation manager
     */
    TranslationManager getTranslationManager();

    /**
     * Get plugin's scheduler adapter
     *
     * @return scheduler adapter instance
     */
    SchedulerAdapter getSchedulerAdapter();

    /**
     * Get plugin's dependency manager
     *
     * @return dependency manager instance
     */
    DependencyManager getDependencyManager();

    /**
     * Get plugin's logger
     *
     * @return milka logger instance
     */
    MilkaLogger getMilkaLogger();

    /**
     * Register YMLFileWrapper
     *
     * @param wrapperClass         wrapper's class
     * @param constructorArguments wrapper's constructor arguments
     * @param <T>                  wrapper's class type
     * @return wrapper instance
     */
    default <T extends YmlFileWrapper> T registerConfigWrapper(Class<T> wrapperClass, Object... constructorArguments) {
        return (T) getConfigurationManager().register(wrapperClass, constructorArguments);
    }

    /**
     * Method called to disable plugin
     */
    void disable();

    /**
     * Unload scheduler
     */
    default void unloadScheduler() {
        getSchedulerAdapter().shutdownScheduler();
        getSchedulerAdapter().shutdownExecutor();
    }
}
