package me.sharkz.milkalib.core.objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface Nameable {

    /**
     * Set object's name
     *
     * @param name object's new name
     */
    void setName(String name);

    /**
     * Get object's name
     *
     * @return object's name
     */
    String getName();
}
