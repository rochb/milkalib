package me.sharkz.milkalib.core.commands;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.utils.MilkaUtils;

import java.util.*;


/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@Setter(AccessLevel.PROTECTED)
public abstract class MilkaCommand extends MilkaUtils {

    private final String key;
    private final List<String> aliases = new ArrayList<>();
    private String permission;
    private String description;

    private final List<MilkaCommand> subCommands = new ArrayList<>();

    private SenderType allowedSenderType = SenderType.BOTH;

    @Setter(AccessLevel.PUBLIC)
    private MilkaCommand parent;

    private final Map<String, Class<?>> requiredArguments = new LinkedHashMap<>();
    private final Map<String, Class<?>> optionalArguments = new LinkedHashMap<>();

    public MilkaCommand(MilkaPlugin plugin, String key) {
        super(plugin);
        this.key = key;
    }

    public MilkaCommand(MilkaPlugin plugin, String key, String... aliases) {
        super(plugin);
        this.key = key;
        this.aliases.addAll(Arrays.asList(aliases));
    }

    public MilkaCommand(MilkaPlugin plugin, String key, Collection<String> aliases) {
        super(plugin);
        this.key = key;
        this.aliases.addAll(aliases);

    }

    /**
     * Add required argument
     *
     * @param name argument's name
     * @param type argument's class type
     */
    protected void addRequiredArgument(String name, Class<?> type) {
        requiredArguments.put(name, type);
    }

    /**
     * Add optional argument
     *
     * @param name argument's name
     * @param type argument's class type
     */
    protected void addOptionalArgument(String name, Class<?> type) {
        optionalArguments.put(name, type);
    }

    /**
     * Get argument class type by index
     *
     * @param index argument index
     * @return optional of argument class type
     * @throws IllegalAccessException thrown if argument adapter method can't be reached.
     * @throws InstantiationException thrown if argument adapter can't be instantiated.
     */
    public Optional<Class<?>> getArgumentType(int index) throws IllegalAccessException, InstantiationException {
        if (requiredArguments.size() > index)
            return Optional.of(new ArrayList<>(requiredArguments.values()).get(index));
        else if (optionalArguments.size() > index - requiredArguments.size())
            return Optional.of(new ArrayList<>(optionalArguments.values()).get(index - requiredArguments.size()));
        return Optional.empty();
    }

    /**
     * Add multiple aliases
     *
     * @param aliases string array
     */
    protected void addAliases(String... aliases) {
        this.aliases.addAll(Arrays.asList(aliases));
    }

    /**
     * Add a single aliases
     *
     * @param aliases string
     */
    private void addAliases(String aliases) {
        this.aliases.add(aliases);
    }

    /**
     * Add a single sub command
     *
     * @param milkaCommand MilkaCommand
     */
    protected void addSubCommand(MilkaCommand milkaCommand) {
        this.subCommands.add(milkaCommand);
        milkaCommand.setParent(this);
    }

    /**
     * Add multiple sub commands
     *
     * @param milkaCommands MilkaCommand array
     */
    protected void addSubCommands(MilkaCommand... milkaCommands) {
        for (MilkaCommand milkaCommand : milkaCommands)
            addSubCommand(milkaCommand);
    }

    /**
     * Get argument by index
     *
     * @param args  arguments array
     * @param index argument index
     * @param type  argument type
     * @param <T>   command type
     * @return optional of argument
     */
    protected <T> Optional<T> getArgument(Object[] args, int index, Class<T> type) {
        if (args.length <= index || !type.isInstance(args[index])) return Optional.empty();
        return Optional.of(type.cast(args[index]));
    }

    protected <T> Optional<T> getArgument(Object[] args, Class<T> type) {
        return (Optional<T>) Arrays.stream(args).filter(type::isInstance).findFirst();
    }

    /**
     * Get command syntax
     *
     * @return command syntax
     */
    public String getSyntax() {
        StringBuilder syntax = new StringBuilder();
        if (Objects.nonNull(parent)) syntax.append(parent.getSyntax());
        syntax.append(key).append(" ");
        requiredArguments.keySet().forEach(s -> syntax.append("[").append(s).append("] "));
        optionalArguments.keySet().forEach(s -> syntax.append("(").append(s).append(") "));
        return syntax.toString();
    }
}
