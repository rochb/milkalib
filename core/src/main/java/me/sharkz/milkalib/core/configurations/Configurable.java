package me.sharkz.milkalib.core.configurations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation Configurable.
 * Used to fill attributes in a {@link IFileWrapper} class.
 *
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Configurable {

    /**
     * Custom key in the config YML file
     *
     * @return The config key
     */
    String key() default "";

}
