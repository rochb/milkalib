package me.sharkz.milkalib.core.utils;

import java.util.LinkedList;
import java.util.TreeMap;

/**
 * @author LBuke (Teddeh)
 */
public class StringUtil {

    private static final int CENTER_PX = 154;
    private static final int CENTER_MENU_PX = 80;
    private final static TreeMap<Integer, String> map = new TreeMap<Integer, String>();

    static {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");
    }

    public static int getPixelLength(String input) {
        input = MilkaColor.translate(input);

        int pixels = 0;
        boolean previousCode = false;
        boolean isBold = false;

        for (char c : input.toCharArray()) {
            if (c == MilkaColor.COLOR_CHAR)
                previousCode = true;
            else if (previousCode) {
                previousCode = false;
                isBold = c == 'l' || c == 'L';
            } else {
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                pixels += isBold ? dFI.getBoldLength() : dFI.getLength();
                pixels++;
            }
        }

        return pixels;
    }

    private static String getCentered(String input, int value) {
        int halvedMessageSize = getPixelLength(input) / 2;
        int toCompensate = value - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder sb = new StringBuilder();
        while (compensated < toCompensate) {
            sb.append(" ");
            compensated += spaceLength;
        }
        return sb.toString() + input;
    }

    public static String getCenteredMessage(String input) {
        return getCentered(input, CENTER_PX);
    }

    public static String getCenteredMenuText(String input) {
        return getCentered(input, CENTER_MENU_PX);
    }

    public static String[] getMultiLinedTextArray(String input, int maxPixelsPerLine) {
        return getMultiLinedTextArray(input, maxPixelsPerLine, "");
    }

    public static String[] getMultiLinedTextArray(String input, int maxPixelsPerLine, String linePrefix) {
        LinkedList<String> list = getMultiLinedText(input, maxPixelsPerLine, linePrefix);
        String[] array = new String[list.size()];
        array = list.toArray(array);
        return array;
    }

    public static LinkedList<String> getMultiLinedText(String input, int maxPixelsPerLine, String linePrefix) {
        input = MilkaColor.translate(input);

        LinkedList<String> splitResult = new LinkedList<>();
        String temp = "";
        int pixels = 0;
        boolean previousCode = false;
        boolean isBold = false;

        for (char c : input.toCharArray()) {
            if (pixels >= maxPixelsPerLine && (c == ' ' || c == '.')) {
                splitResult.add(linePrefix + temp);
                temp = MilkaColor.getLastColors(temp) + "";
                pixels = 0;
                continue;
            }

            temp += c;

            if (c == MilkaColor.COLOR_CHAR) {
                previousCode = true;
                continue;
            } else if (previousCode) {
                previousCode = false;
                if (c == 'l' || c == 'L') {
                    isBold = true;
                    continue;
                }

                isBold = false;
                continue;
            }

            DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
            pixels += isBold ? dFI.getBoldLength() : dFI.getLength();
            pixels++;
        }

        splitResult.add(linePrefix + temp);

        return splitResult;
    }

    public static String getProgressBar(int current, int max, int totalBars, char symbol) {
        return getProgressBar(current, max, totalBars, symbol, MilkaColor.GREEN, MilkaColor.GRAY, false);
    }

    public static String getProgressBar(int current, int max, int totalBars, char symbol, String completedColor, String notCompletedColor) {
        return getProgressBar(current, max, totalBars, symbol, completedColor, notCompletedColor, false);
    }

    public static String getProgressBar(int current, int max, int totalBars, char symbol, String completedColor, String notCompletedColor, boolean reverse) {
        int progressBars = (int) (totalBars * (float) current / max);
        int leftOver = (totalBars - progressBars);

        StringBuilder sb = new StringBuilder();
        sb.append(MilkaColor.translate(completedColor));
        for (int i = 0; i < (reverse ? leftOver : progressBars); i++) {
            sb.append(symbol);
        }

        sb.append(MilkaColor.translate(notCompletedColor));
        for (int i = 0; i < (reverse ? progressBars : leftOver); i++) {
            sb.append(symbol);
        }

        return sb.toString();
    }

    /**
     * Latin numeral to roman numeral levels.
     */
    public static String toRoman(int number) {
        int l = map.floorKey(number);
        if (number == l) {
            return map.get(number);
        }
        return map.get(l) + toRoman(number - l);
    }

    public static String singular(String... input) {
        if (input == null)
            throw new NullPointerException("The given input was null..");

        // Simply return given string if array only
        // contains a single element.
        if (input.length <= 1)
            return input[0];

        // If array contains multiple elements, a new
        // StringBuilder object is made.
        // Only needed for compiling multiple elements.
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < (input.length - 1); i++)
            builder.append(input[i]).append('\n');
        builder.append(input[input.length - 1]);
        return builder.toString();
    }

    /**
     * Get the percentage between two points.
     *
     * @param current - Let's assume this is <b>5</b>
     * @param max     - Let's assume this is <b>10</b>
     * @return The result would be <b>50</b>%
     */
    public static String getPercentage(int current, int max, String numberColor, String symbolColor) {
        return String.format("%s%s&r%s%s", MilkaColor.translate(numberColor), (int) (Math.round(((float) current * 100 / max) * 10.0) / 10.0), MilkaColor.translate(symbolColor), "%");
    }

    public static String getPercentage(int current, int max) {
        return String.format("%s%s", (int) (Math.round(((float) current * 100 / max) * 10.0) / 10.0), "%");
    }
}
