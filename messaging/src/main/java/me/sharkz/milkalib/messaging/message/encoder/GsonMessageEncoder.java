package me.sharkz.milkalib.messaging.message.encoder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.sharkz.milkalib.messaging.message.Message;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class GsonMessageEncoder implements MessageEncoder {

    private final Gson gson = new GsonBuilder()
            .enableComplexMapKeySerialization()
            .create();

    public String encode(Message message) {
        JSONObject object = new JSONObject();
        object.put("name", message.getClass().getName());
        object.put("payload", gson.toJson(message));
        return object.toString();
    }

    public Optional<Message> decode(String data) throws ParseException {
        JSONObject object = (JSONObject) new JSONParser().parse(data);
        String name = (String) object.get("name");
        String payload = (String) object.get("payload");
        try {
            Class<?> payloadClass = Class.forName(name);
            return (Optional<Message>) Optional.ofNullable(gson.fromJson(payload, payloadClass));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
