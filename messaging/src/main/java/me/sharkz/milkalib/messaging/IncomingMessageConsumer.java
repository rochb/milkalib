package me.sharkz.milkalib.messaging;

import lombok.NonNull;
import me.sharkz.milkalib.messaging.message.Message;
import me.sharkz.milkalib.messaging.message.OutgoingMessage;
import me.sharkz.milkalib.messaging.message.encoder.GsonMessageEncoder;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface IncomingMessageConsumer {

    /**
     * Consumes a message instance.
     *
     * <p>The boolean returned from this method indicates whether or not the
     * platform accepted the message. Some implementations which have multiple
     * distribution channels may wish to use this result to dispatch the same
     * message back to additional receivers.</p>
     *
     * <p>The implementation will usually return <code>false</code> if a message
     * with the same ping id has already been processed.</p>
     *
     * @param message the message
     */
    void consumeIncomingMessage(@NonNull Message message);

    /**
     * Consumes a message in an encoded string format.
     *
     * <p>This method will decode strings obtained by calling
     * {@link OutgoingMessage#asEncodedString()}. This means that basic
     * implementations can successfully implement {@link Messenger} without
     * providing their own serialisation.</p>
     *
     * <p>The boolean returned from this method indicates whether or not the
     * platform accepted the message. Some implementations which have multiple
     * distribution channels may wish to use this result to dispatch the same
     * message back to additional receivers.</p>
     *
     * <p>The implementation will usually return <code>false</code> if a message
     * with the same ping id has already been processed.</p>
     *
     * @param encodedString the encoded string
     */
    default void consumeIncomingMessageAsString(@NonNull String encodedString) {
        try {
            new GsonMessageEncoder().decode(encodedString).ifPresent(this::consumeIncomingMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
