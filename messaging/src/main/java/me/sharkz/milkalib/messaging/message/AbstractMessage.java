package me.sharkz.milkalib.messaging.message;

import lombok.RequiredArgsConstructor;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public abstract class AbstractMessage implements Message, OutgoingMessage {

    private final UUID id;

    public AbstractMessage() {
        this.id = UUID.randomUUID();
    }

    @Override
    public UUID getUniqueId() {
        return id;
    }
}
