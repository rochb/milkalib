package me.sharkz.milkalib.messaging.rabbitmq;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.rabbitmq.client.*;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.messaging.IncomingMessageConsumer;
import me.sharkz.milkalib.messaging.Messenger;
import me.sharkz.milkalib.messaging.message.OutgoingMessage;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class RabbitMQMessenger implements Messenger {

    private static final boolean CHANNEL_PROP_DURABLE = false;
    private static final boolean CHANNEL_PROP_EXCLUSIVE = true;
    private static final boolean CHANNEL_PROP_AUTO_DELETE = true;

    private final MilkaPlugin plugin;
    private final IncomingMessageConsumer consumer;

    private ConnectionFactory connectionFactory;
    private Connection connection;
    private Channel channel;
    private Subscription sub;

    private String EXCHANGE;
    private String ROUTING_KEY;

    public void init(String host, int port, String virtualHost, String username, String password, String EXCHANGE, String ROUTING_KEY) {
        this.connectionFactory = new ConnectionFactory();
        this.connectionFactory.setHost(host);
        this.connectionFactory.setPort(port);
        this.connectionFactory.setVirtualHost(virtualHost);
        this.connectionFactory.setUsername(username);
        this.connectionFactory.setPassword(password);

        this.EXCHANGE = EXCHANGE;
        this.ROUTING_KEY = ROUTING_KEY;

        this.sub = new Subscription();
        plugin.getSchedulerAdapter().executeAsync(this.sub);
    }

    @Override
    public void sendOutgoingMessage(@NonNull OutgoingMessage outgoingMessage) {
        try {
            ByteArrayDataOutput output = ByteStreams.newDataOutput();
            output.writeUTF(outgoingMessage.asEncodedString());
            this.channel.basicPublish(EXCHANGE, ROUTING_KEY, new AMQP.BasicProperties.Builder().build(), output.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        try {
            this.channel.close();
            this.connection.close();
            this.sub.isClosed = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks the connection, and re-opens it if necessary.
     *
     * @return true if the connection is now alive, false otherwise
     */
    private boolean checkAndReopenConnection() {
        boolean connectionAlive = this.connection != null && this.connection.isOpen();
        boolean channelAlive = this.channel != null && this.channel.isOpen();

        if (connectionAlive && channelAlive) {
            return true;
        }

        if (this.channel != null && this.channel.isOpen()) {
            try {
                this.channel.close();
            } catch (Exception ignored) {
            }
        }
        if (this.connection != null && this.connection.isOpen()) {
            try {
                this.connection.close();
            } catch (Exception ignored) {
            }
        }

        try {
            this.connection = this.connectionFactory.newConnection();
            this.channel = this.connection.createChannel();

            String queue = this.channel.queueDeclare("", CHANNEL_PROP_DURABLE, CHANNEL_PROP_EXCLUSIVE, CHANNEL_PROP_AUTO_DELETE, null).getQueue();
            this.channel.exchangeDeclare(EXCHANGE, BuiltinExchangeType.TOPIC, CHANNEL_PROP_DURABLE, CHANNEL_PROP_AUTO_DELETE, null);
            this.channel.queueBind(queue, EXCHANGE, ROUTING_KEY);
            this.channel.basicConsume(queue, true, this.sub, tag -> {
            });
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }


    private class Subscription implements Runnable, DeliverCallback {
        private boolean isClosed = false;

        @Override
        public void run() {
            while (!Thread.interrupted() && !this.isClosed) {
                try {
                    if (!checkAndReopenConnection()) {
                        Thread.sleep(5000);
                        continue;
                    }

                    Thread.sleep(30_000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }

        @Override
        public void handle(String consumerTag, Delivery message) {
            byte[] data = message.getBody();
            ByteArrayDataInput input = ByteStreams.newDataInput(data);
            String msg = input.readUTF();
            RabbitMQMessenger.this.consumer.consumeIncomingMessageAsString(msg);
        }
    }
}
