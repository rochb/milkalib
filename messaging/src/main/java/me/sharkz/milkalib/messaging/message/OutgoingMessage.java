package me.sharkz.milkalib.messaging.message;

import lombok.NonNull;
import me.sharkz.milkalib.messaging.message.encoder.GsonMessageEncoder;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface OutgoingMessage extends Message {

    /**
     * Gets an encoded string data form of this message.
     *
     * @return an encoded string form of the message
     */
    @NonNull
    default String asEncodedString() {
        return new GsonMessageEncoder().encode(this);
    }
}
