package me.sharkz.milkalib.messaging.message;

import me.sharkz.milkalib.core.objects.Unique;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface Message extends Unique {
}
