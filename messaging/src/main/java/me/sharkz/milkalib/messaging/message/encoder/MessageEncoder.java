package me.sharkz.milkalib.messaging.message.encoder;

import me.sharkz.milkalib.messaging.message.Message;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface MessageEncoder {

    String encode(Message message);

    Optional<Message> decode(String data) throws Exception;
}
