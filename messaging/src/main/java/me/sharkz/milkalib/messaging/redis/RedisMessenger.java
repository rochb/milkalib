package me.sharkz.milkalib.messaging.redis;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.messaging.IncomingMessageConsumer;
import me.sharkz.milkalib.messaging.Messenger;
import me.sharkz.milkalib.messaging.message.OutgoingMessage;
import redis.clients.jedis.*;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class RedisMessenger implements Messenger {

    private final MilkaPlugin plugin;
    private final String channel;
    private final IncomingMessageConsumer consumer;

    private JedisPool jedisPool;
    private Subscription subscription;

    public void init(String address, int port, String password, boolean ssl) {

        if (Objects.isNull(password) || password.isEmpty())
            this.jedisPool = new JedisPool(new JedisPoolConfig(), address, port, Protocol.DEFAULT_TIMEOUT, ssl);
        else
            this.jedisPool = new JedisPool(new JedisPoolConfig(), address, port, Protocol.DEFAULT_TIMEOUT, password, ssl);

        this.subscription = new Subscription(channel);
        plugin.getSchedulerAdapter().executeAsync(this.subscription);
    }

    @Override
    public void sendOutgoingMessage(@NonNull OutgoingMessage outgoingMessage) {
        try (Jedis jedis = this.jedisPool.getResource()) {
            jedis.publish(channel, outgoingMessage.asEncodedString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        this.subscription.unsubscribe();
        this.jedisPool.destroy();
    }

    @RequiredArgsConstructor
    private class Subscription extends JedisPubSub implements Runnable {

        private final String channel;

        @Override
        public void run() {
            boolean wasBroken = false;
            while (!Thread.interrupted() && !RedisMessenger.this.jedisPool.isClosed()) {
                try (Jedis jedis = RedisMessenger.this.jedisPool.getResource()) {
                    if (wasBroken) {
                        wasBroken = false;
                    }
                    jedis.subscribe(this, this.channel);
                } catch (Exception e) {
                    wasBroken = true;
                    try {
                        unsubscribe();
                    } catch (Exception ignored) {
                    }

                    // Sleep for 5 seconds to prevent massive spam in console
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ie) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        @Override
        public void onMessage(String channel, String msg) {
            if (!channel.equals(this.channel)) return;
            RedisMessenger.this.consumer.consumeIncomingMessageAsString(msg);
        }
    }
}
