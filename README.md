# MilkaLib

**Useful library for dumb developers.**

Developed by Roch Blondiaux.
www.roch-blondiaux.com

## Adding MilkaLib to your project

Import with maven

    <repository>  
    	 <id>rb-releases</id>  
    	 <url>http://185.216.25.89:8081/repository/maven-releases/</url>  
    </repository>

    <dependency>
    	<groupId>me.sharkz</groupId>  
    	<artifactId>milkalib</artifactId>  
    	<version>2.0</version>
    	<scope>provided</scope>
    </dependency>

Import with gradle

    repositories {
    	maven {
    		url = 'http://185.216.25.89:8081/repository/maven-releases/'
    	}
    }
    dependencies  {                                                        
    	compileOnly 'me.sharkz:milkalib:2.0'                          
    }   

## Commands

### Hello World!

    public class HelloWorldCmd extends SpigotCommand {  
      
      public HelloWorldCmd() {  
	      super("helloworld");  
      
	      setDescription("Hello world command");  
	      setPermission("my.permission");  
	      setAllowedSenderType(SenderType.CONSOLE);  
      }  
      
      @Override  
      protected CommandResult execute(CommandSender sender, Object[] args) {  
	     sender.sendMessage("Hello world!");  
	      return CommandResult.SUCCESS;  
      }  
    }

### Sub commands

    MilkaCommand::addSubCommand(MilkaCommand)

### Registration

    SpigotPlugin::registerCommand(MyCmd.class)

## UI

### Buttons

### Fillers

### Registration

    SpigotPlugin::registerUI(MyUI.class)

### Open

    UIManager::open(Player player, Class<? extends MilkaUI> uiClass, Object... objects);

### Hello World!

    public class HelloWorldUI extends MilkaUI {  
      
      public HelloWorldUI(MyPlugin plugin) {  
	      super(plugin);  
	      createInventory("&aHello &b&lWorld &d!", 54);  
      }  
      
      @Override  
      public void open(Player player, Object[] objects) {  
      
	      addItem(15, new ItemBuilder(Material.NETHERITE_SWORD)  
		      .setName("&aHi &bThere &c!")  
		      .setLore("&r",  
		      "&aWhat's",  
		      "&bUP ?"))  
		      .setLeftClickAction(event -> player.sendMessage("Hi " + player.getName()));  
      
	      fillBorders(new ItemBuilder(Material.RED_STAINED_GLASS_PANE))  
		      .forEach(uiButton -> uiButton.setKeyboardClickAction(event -> player.sendMessage("Keyboard click!")));  
      }  
    }

### Dynamic UI

### Paginated UI

## Listeners

## ItemBuilder

## Skull Creator

## Configuration Wrapper

## Adapters

MilkaAdapters are useful to parse objects from string. They're mainly used to parse command's arguments.

### Usage

    AdaptersManager.getAdapter(Class<?> type)
	    .ifPresent(adapter -> {
		Optional<Class<?>> parsedObject = adapter.parse("my data");	    
	    });

### Getting started

First of all, create a new class named ***ObjectType*Adapter**

     public class IntegerAdapter implements MilkaAdapter<Integer> {  
      
      @Override  
      public Optional<Integer> parse(String data) {  
        try {  
	        return Optional.of(Integer.parseInt(data));  
        } catch (Exception e) {  
	        return Optional.empty();  
        }  
     }  

      @Override  
      public Collection<String> getSuggestions() {  
	      return Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100");  
      }  
    }

Then, register it:

    AdaptersManager::register(ObjectTypeAdapter.class)

## YAML Serializers

YAML Serializers are useful to serialize and deserialize objects from/to configuration sections.

### Usage

    LocationSerializer serializer = new LocationSerializer();  
    
    serializer.serialize(section, location);  
    
    serializer.deserialize(section)  
      .ifPresent(location -> player.teleport(location));

### Getting started

First of all create a class named ***ObjectType*Serializer**

      public class LocationSerializer implements YMLSerializer<Location> {  
      
      
      @Override  
      public Optional<Location> deserialize(@NonNull ConfigurationSection section) throws YMLDeserializationException {  
	      if (!section.isDouble("x")  
	            || !section.isDouble("y")  
	            || !section.isDouble("z")  
	            || !section.isString("world")) 
		        throw new YMLDeserializationException("Missing location part!");  
		      
	      double x = section.getDouble("x");  
	      double y = section.getDouble("y");  
	      double z = section.getDouble("z");  
      
	      float yaw = 0;  
	      if(section.isDouble("yaw"))  
	      yaw = (float) section.getDouble("yaw");  
      
	      float pitch = 0;  
	      if(section.isDouble("pitch"))  
	      pitch = (float) section.getDouble("pitch");  
      
	      String worldName = section.getString("world", "world");  
	      World world = Bukkit.getWorld(worldName);  
	      if (Objects.isNull(world)) 
		      throw new YMLDeserializationException("World " + worldName + " doesn't exists!");  
      
	      return Optional.of(new Location(world, x, y, z, yaw, pitch));  
      }  
      
      @Override  
      public void serialize(ConfigurationSection section, @NonNull Location object) {  
	      section.set("x", object.getX());  
	      section.set("y", object.getY());  
	      section.set("z", object.getZ());  
	      if(object.getYaw() != 0) 
		      section.set("yaw", object.getYaw());  
	      if(object.getPitch() != 0) 
		      section.set("pitch", object.getPitch());  
	      section.set("world", Objects.requireNonNull(object.getWorld()).getName());  
      }  

    }

Then register it:

    ConfigurationManager::getSerializersManager().register(ObjectTypeSerializer.class)

## Unique Object

## Nameable Object

## Objects Manager

