package me.sharkz.milkalib.bungeecord.configuration;

import lombok.RequiredArgsConstructor;
import me.sharkz.milkalib.core.configurations.wrapper.YamlFileLoadException;
import me.sharkz.milkalib.core.configurations.wrapper.YmlFileWrapper;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class FileWrapper implements YmlFileWrapper {

    /**
     * Java file object representation
     */
    private final File file;

    /**
     * URL of a default resource to use when initializing the file
     */
    private final URL defaultResource;

    /**
     * Configuration object used to retrieve and format the config file
     */
    protected Configuration configuration;

    /**
     * Constructs a new configuration file wrapper without default resource.
     *
     * @param file Java file object
     */
    public FileWrapper(File file) {
        this(file, null);
    }

    /**
     * Constructs a new configuration file wrapper from file name
     *
     * @param plugin   bungeecord plugin
     * @param fileName file's name
    public FileWrapper(BungeePlugin plugin, String fileName) {
    if (!fileName.endsWith(".yml")) fileName += ".yml";
    this.file = new File(plugin.getDataFolder(), fileName);
    this.defaultResource = plugin.getClass().getClassLoader().getResource(fileName);
    }
     */


    /**
     * Retrieves the configuration object.
     *
     * @return configuration object
     */
    public Configuration get() {
        if (Objects.isNull(configuration))
            throw new NullPointerException("configuration object of the file is null");
        return configuration;
    }

    @Override
    public void load() throws YamlFileLoadException {
        try {
            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
        } catch (IOException e) {
            throw new YamlFileLoadException("Cannot copy the default resource on the disk", e);
        }
    }

    @Override
    public void save() throws IOException {
        if (Objects.nonNull(configuration))
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, file);
    }

    @Override
    public boolean exists() {
        return file.exists();
    }

    @Override
    public InputStream getDefaultResourceStream() throws IOException {
        URLConnection connection = defaultResource.openConnection();
        connection.setUseCaches(false);
        return connection.getInputStream();
    }
}
