package me.sharkz.milkalib.bungeecord.configuration;

import me.sharkz.milkalib.core.configurations.Configurable;
import me.sharkz.milkalib.core.configurations.IFileWrapper;
import me.sharkz.milkalib.core.configurations.wrapper.YamlFileLoadException;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.Objects;

/**
 * Represents a dynamically configurable
 * wrapper for a configuration file on the disk.
 *
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class ConfigurableFileWrapper extends FileWrapper implements IFileWrapper {

    /**
     * Constructs the configurable file wrapper.
     *
     * @param file file object
     */
    protected ConfigurableFileWrapper(File file) {
        super(file);
    }

    /**
     * Constructs the configurable file wrapper with a default resource.
     *
     * @param file            file object
     * @param defaultResource default resource URL
     */
    private ConfigurableFileWrapper(File file, URL defaultResource) {
        super(file, defaultResource);
    }

    /**
     * Loads the configuration file from the disk.
     * Fills attributes of the instance with values from the file.
     */
    @Override
    public void load() throws YamlFileLoadException {
        super.load();

        // Load every needed config value dynamically!
        for (Field field : this.getClass().getDeclaredFields()) {
            Configurable conf = field.getAnnotation(Configurable.class);
            if (Objects.isNull(conf)) continue;

            // Getting the config key associated with the field
            String configKey = (conf.key().isEmpty()) ? field.getName() : conf.key();
            Object value = this.configuration.get(configKey);

            // Changing the value of the field
            try {
                field.setAccessible(true);
                field.set(this, value);
                field.setAccessible(false);
            } catch (ReflectiveOperationException | IllegalArgumentException e) {
                String configName = getClass().getSimpleName().toLowerCase();
                throw new YamlFileLoadException(String.format(
                        "Cannot set the config value %s of key %s for %s",
                        value, configKey, configName
                ), e);
            }
        }
    }

}
