package me.sharkz.milkalib.spigot.dialog;

import me.sharkz.milkalib.core.translations.TranslationPack;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.utils.SpigotUtils;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class DialogBuilder extends SpigotUtils {

    private final SpigotPlugin plugin;

    private Player player;
    private List<String> text;
    private Dialog.DialogValidator validator;
    private BiConsumer<Player, String> complete;
    private Consumer<Player> cancel;
    private boolean cancellable;

    public DialogBuilder(SpigotPlugin plugin) {
        super(plugin);
        this.plugin = plugin;
    }

    public DialogBuilder setPlayer(Player player) {
        this.player = player;
        return this;
    }

    public <T extends TranslationPack> DialogBuilder setText(T translation, Object... placeholders) {
        this.text = new ArrayList<>(format(translation, placeholders));
        return this;
    }

    public <T extends TranslationPack> DialogBuilder setText(T translation) {
        this.text = translate(translation);
        return this;
    }

    public DialogBuilder setText(String... text) {
        this.text = Arrays.asList(text);
        return this;
    }

    public DialogBuilder setText(List<String> text, Object... placeholders) {
        this.text = new ArrayList<>(format(text, placeholders));
        return this;
    }

    public DialogBuilder setText(List<String> text) {
        this.text = text;
        return this;
    }

    public DialogBuilder setValidator(Dialog.DialogValidator validator) {
        this.validator = validator;
        return this;
    }

    public DialogBuilder onComplete(BiConsumer<Player, String> complete) {
        this.complete = complete;
        return this;
    }

    public DialogBuilder onCancel(Consumer<Player> cancel) {
        this.cancel = cancel;
        return this;
    }

    public DialogBuilder setCancellable(boolean cancellable) {
        this.cancellable = cancellable;
        return this;
    }

    public Dialog build() {
        return new Dialog(plugin, player, text, validator, complete, cancel, cancellable);
    }
}
