package me.sharkz.milkalib.spigot.commands.integrated;

import me.sharkz.milkalib.core.commands.CommandResult;
import me.sharkz.milkalib.core.utils.StringUtil;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.commands.SpigotCommand;
import org.bukkit.command.CommandSender;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class VersionCommand extends SpigotCommand {

    public VersionCommand(SpigotPlugin plugin) {
        super(plugin, "version", "v");
        setDescription("Displays plugin's version.");
    }

    @Override
    protected CommandResult execute(CommandSender sender, Object[] args) {
        sender.sendMessage(color(StringUtil.getCenteredMessage("&f&m                                    ")));
        sender.sendMessage(color(StringUtil.getCenteredMessage("&b&l" + getPlugin().getName() + " &f&lv" + getSpigotPlugin().getDescription().getVersion())));
        sender.sendMessage(color(StringUtil.getCenteredMessage("&f&lDeveloped by &b&l" + getAuthors(getSpigotPlugin()))));
        sender.sendMessage(color(StringUtil.getCenteredMessage("&f&m                                    ")));
        return CommandResult.SUCCESS;
    }

    private String getAuthors(SpigotPlugin plugin) {
        StringBuilder builder = new StringBuilder();
        plugin.getDescription()
                .getAuthors()
                .forEach(s -> builder.append(s).append(","));
        String b = builder.toString();
        return b.substring(0, b.length() - 2);
    }
}
