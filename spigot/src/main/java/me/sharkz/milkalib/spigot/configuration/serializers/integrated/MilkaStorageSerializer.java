package me.sharkz.milkalib.spigot.configuration.serializers.integrated;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import me.sharkz.milkalib.core.storage.MilkaStorage;
import me.sharkz.milkalib.core.storage.StorageCredentials;
import me.sharkz.milkalib.core.storage.StorageFactory;
import me.sharkz.milkalib.core.storage.StorageType;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.configuration.serializers.YMLSerializer;
import me.sharkz.milkalib.spigot.exceptions.YMLSerializationException;
import org.bukkit.configuration.ConfigurationSection;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class MilkaStorageSerializer implements YMLSerializer<MilkaStorage> {

    private final SpigotPlugin plugin;

    private static final StorageCredentialsSerializer credentialsSerializer = new StorageCredentialsSerializer();

    @Override
    public Optional<MilkaStorage> deserialize(@NonNull ConfigurationSection section) {
        if (Objects.isNull(section.getParent()) || !section.getParent().isString("storage-method"))
            return Optional.empty();
        Optional<StorageType> type = Arrays.stream(StorageType.values())
                .filter(type1 -> type1.name().equalsIgnoreCase(section.getParent().getString("storage-method")))
                .findFirst();
        Optional<StorageCredentials> credentials = credentialsSerializer.deserialize(section);
        return type.flatMap(type1 -> credentials.flatMap(credentials1 -> Optional.of(new StorageFactory(plugin, type1, credentials1).build())));
    }

    @Override
    public void serialize(ConfigurationSection section, @NonNull MilkaStorage object) throws YMLSerializationException {

    }
}
