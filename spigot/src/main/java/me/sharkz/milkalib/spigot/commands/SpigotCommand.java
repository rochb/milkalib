package me.sharkz.milkalib.spigot.commands;

import me.sharkz.milkalib.core.commands.CommandResult;
import me.sharkz.milkalib.core.commands.MilkaCommand;
import me.sharkz.milkalib.core.translations.TranslationPack;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.ui.MilkaUI;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.Permission;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.Collection;
import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class SpigotCommand extends MilkaCommand {

    public SpigotCommand(SpigotPlugin plugin, String key) {
        super(plugin, key);
    }

    public SpigotCommand(SpigotPlugin plugin, String key, String... aliases) {
        super(plugin, key, aliases);
    }

    public SpigotCommand(SpigotPlugin plugin, String key, Collection<String> aliases) {
        super(plugin, key, aliases);
    }

    protected abstract CommandResult execute(CommandSender sender, Object[] args);

    /**
     * Run synchronous task
     *
     * @param runnable to run
     * @return bukkit task
     */
    protected BukkitTask runSync(BukkitRunnable runnable) {
        return runnable.runTask(getSpigotPlugin());
    }

    /**
     * Run repeated synchronous task
     *
     * @param runnable to run
     * @param delay    before running
     * @param period   between each run
     * @return bukkit task
     */
    protected BukkitTask runRepeatedSync(BukkitRunnable runnable, long delay, long period) {
        return runnable.runTaskTimer(getSpigotPlugin(), delay, period);
    }

    /**
     * Run synchronous task later
     *
     * @param runnable to run
     * @param delay    before running
     * @return bukkit task
     */
    protected BukkitTask runSyncLater(BukkitRunnable runnable, long delay) {
        return runnable.runTaskLater(getSpigotPlugin(), delay);
    }

    /**
     * Run asynchronous task
     *
     * @param runnable to run
     * @return bukkit task
     */
    protected BukkitTask runAsync(BukkitRunnable runnable) {
        return runnable.runTaskAsynchronously(getSpigotPlugin());
    }

    /**
     * Run repeated asynchronous task
     *
     * @param runnable to run
     * @param delay    before running
     * @param period   between each run
     * @return bukkit task
     */
    protected BukkitTask runRepeatedAsync(BukkitRunnable runnable, long delay, long period) {
        return runnable.runTaskTimerAsynchronously(getSpigotPlugin(), delay, period);
    }

    /**
     * Run asynchronous task later
     *
     * @param runnable to run
     * @param delay    before running
     * @return bukkit task
     */
    protected BukkitTask runAsyncLater(BukkitRunnable runnable, long delay) {
        return runnable.runTaskLaterAsynchronously(getSpigotPlugin(), delay);
    }

    /**
     * Check if permissible has permission
     *
     * @param permissible permissible to check
     * @param permission  permission to have
     * @return true if's the case
     */
    protected boolean hasPermission(Permissible permissible, String permission) {
        return permissible.hasPermission(permission);
    }

    /**
     * Check if permissible has permission
     *
     * @param permissible permissible to check
     * @param permission  permission to have
     * @return true if's the case
     */
    protected boolean hasPermission(Permissible permissible, Permission permission) {
        return permissible.hasPermission(permission);
    }

    /**
     * Open ui to player
     *
     * @param player  targeted player
     * @param ui      ui to open
     * @param objects objects to pass trough ui open method
     */
    protected void openUI(Player player, Class<? extends MilkaUI> ui, Object... objects) {
        getSpigotPlugin().getUiManager().open(player, ui, objects);
    }

    /**
     * Send translated & colored message to command sender
     *
     * @param sender      targeted command sender
     * @param translation translation entry
     * @param <T>         translation class
     */
    protected <T extends TranslationPack> void message(CommandSender sender, T translation) {
        color(translation).forEach(sender::sendMessage);
    }

    /**
     * Send translated, formatted & colored message to command sender
     *
     * @param sender       targeted command sender
     * @param translation  translation entry
     * @param placeholders placeholders map
     * @param <T>          translation class
     */
    protected <T extends TranslationPack> void message(CommandSender sender, T translation, Map<String, String> placeholders) {
        colorAndFormat(translation).forEach(sender::sendMessage);
    }

    /**
     * Send translated, formatted & colored message to command sender
     *
     * @param sender       targeted command sender
     * @param translation  translation entry
     * @param placeholders placeholders
     * @param <T>          translation class
     */
    protected <T extends TranslationPack> void message(CommandSender sender, T translation, Object... placeholders) {
        colorAndFormat(translation, placeholders).forEach(sender::sendMessage);

    }

    /**
     * Give or drop a single itemstack
     *
     * @param player    targeted player
     * @param itemStack itemstack to give
     */
    protected void giveOrDrop(Player player, ItemStack itemStack) {
        if (player.getInventory().firstEmpty() == -1)
            player.getWorld().dropItem(player.getLocation(), itemStack);
        else
            player.getInventory().addItem(itemStack);
    }

    /**
     * Give or drop multiples itemstacks
     *
     * @param player     targeted player
     * @param itemStacks itemstacks array to give
     */
    protected void giveOrDrop(Player player, ItemStack... itemStacks) {
        for (ItemStack itemStack : itemStacks)
            giveOrDrop(player, itemStack);
    }

    /**
     * Give or drop multiples itemstacks
     *
     * @param player     targeted player
     * @param itemStacks itemstacks collections to give
     */
    protected void giveOrDrop(Player player, Collection<ItemStack> itemStacks) {
        itemStacks.forEach(itemStack -> giveOrDrop(player, itemStack));
    }

    /**
     * Get spigot plugin
     *
     * @return spigot plugin instance
     */
    public SpigotPlugin getSpigotPlugin() {
        return (SpigotPlugin) getPlugin();
    }
}
