package me.sharkz.milkalib.spigot.messaging;

import me.sharkz.milkalib.messaging.message.Message;
import me.sharkz.milkalib.messaging.message.encoder.MessageEncoder;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BukkitMessageEncoder implements MessageEncoder {

    @Override
    public String encode(Message message) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            dataOutput.writeObject(message);
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to encode message.", e);
        }
    }

    @Override
    public Optional<Message> decode(String data) throws Exception {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
        BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
        Message message = (Message) dataInput.readObject();
        dataInput.close();
        return Optional.ofNullable(message);
    }
}
