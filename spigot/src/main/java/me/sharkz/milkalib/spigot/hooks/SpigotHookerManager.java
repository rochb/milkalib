package me.sharkz.milkalib.spigot.hooks;

import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.hooks.HookersManager;
import me.sharkz.milkalib.core.hooks.PluginHooker;
import org.bukkit.Bukkit;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SpigotHookerManager extends HookersManager {

    public SpigotHookerManager(MilkaPlugin plugin) {
        super(plugin);
    }

    @Override
    protected boolean isHooked(PluginHooker pluginHooker) {
        return Objects.nonNull(Bukkit.getPluginManager().getPlugin(pluginHooker.getRequiredPlugin()));
    }
}
