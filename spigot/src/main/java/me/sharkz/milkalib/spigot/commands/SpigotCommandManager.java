package me.sharkz.milkalib.spigot.commands;

import me.sharkz.milkalib.core.commands.CommandsManager;
import me.sharkz.milkalib.core.exceptions.CommandRegistrationException;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginCommand;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SpigotCommandManager extends CommandsManager<SpigotCommand> {

    public SpigotCommandManager(SpigotPlugin plugin) {
        super(plugin);
        setExecutor(new SpigotCommandExecutor(plugin, this, plugin.getAdaptersManager()));
    }

    @Override
    protected boolean registerCommand(SpigotCommand command) throws CommandRegistrationException {
        try {
            Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);

            CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());

            Class<? extends PluginCommand> plCmd = PluginCommand.class;
            Constructor<? extends PluginCommand> plCmdConstructor = plCmd.getDeclaredConstructor(String.class, Plugin.class);
            plCmdConstructor.setAccessible(true);

            SpigotCommandExecutor executor = (SpigotCommandExecutor) getExecutor();

            PluginCommand cmd = plCmdConstructor.newInstance(command.getKey(), getPlugin());
            cmd.setExecutor(executor);
            cmd.setTabCompleter(executor);
            cmd.setAliases(command.getAliases());

            if (Objects.nonNull(command.getPermission()) && !command.getPermission().isEmpty()) {
                String description = "No description.";
                if (Objects.nonNull(command.getDescription()) && !command.getDescription().isEmpty())
                    description = command.getDescription();
                Bukkit.getPluginManager().addPermission(new Permission(command.getPermission(), description));
            }

            return commandMap.register(cmd.getName(), getPlugin().getName(), cmd);
        } catch (Exception e) {
            throw new CommandRegistrationException("Couldn't register command in bukkit command map", e.getCause());
        }
    }

    @Override
    protected boolean unregisterCommand(SpigotCommand command) {
        return false;
    }

    @Override
    public List<SpigotCommand> getCommands() {
        return super.getCommands();
    }
}
