package me.sharkz.milkalib.spigot.ui;

import lombok.Data;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class UIConstructorInfo {

    private final Class<? extends MilkaUI> clazz;
    private final Class<?>[] classes;
    private final Object[] objects;
}
