package me.sharkz.milkalib.spigot.configuration.serializers.integrated;

import com.cryptomorin.xseries.XItemStack;
import lombok.NonNull;
import me.sharkz.milkalib.spigot.configuration.serializers.YMLSerializer;
import me.sharkz.milkalib.spigot.exceptions.YMLDeserializationException;
import me.sharkz.milkalib.spigot.exceptions.YMLSerializationException;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ItemStackSerializer implements YMLSerializer<ItemStack> {

    @Override
    public Optional<ItemStack> deserialize(@NonNull ConfigurationSection section) throws YMLDeserializationException {
        return Optional.ofNullable(XItemStack.deserialize(section));
    }

    @Override
    public void serialize(ConfigurationSection section, @NonNull ItemStack object) throws YMLSerializationException {
        XItemStack.serialize(object, section);
    }

}
