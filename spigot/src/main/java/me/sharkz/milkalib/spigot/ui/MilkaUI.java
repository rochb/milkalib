package me.sharkz.milkalib.spigot.ui;

import lombok.Getter;
import lombok.Setter;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.ui.components.UIButton;
import me.sharkz.milkalib.spigot.utils.SpigotUtils;
import me.sharkz.milkalib.spigot.utils.items.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@Setter
public abstract class MilkaUI extends SpigotUtils {

    private final List<UIButton> buttons = new ArrayList<>();
    private Inventory inventory;
    private String title;

    private final List<Integer> allowedClickSlots = new ArrayList<>();

    private final int[][] borderSlots = {
            {0, 8},
            {0, 8, 9, 17},
            {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26},
            {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35},
            {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44},
            {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53},
    };
    private final int[][] insideSlots = {
            {1, 2, 3, 4, 5, 6, 7},
            {1, 2, 3, 4, 5, 6, 7, 10, 11, 12, 13, 14, 15, 16},
            {10, 11, 12, 13, 14, 15, 16},
            {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25},
            {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34},
            {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43},
    };

    public MilkaUI(SpigotPlugin plugin) {
        super(plugin);
    }

    /**
     * Method called on inventory open
     *
     * @param player  who opens the ui
     * @param objects args
     */
    public abstract void open(Player player, Object[] objects);

    /**
     * Method called on item drag event
     *
     * @param event  bukkit event
     * @param player who drags items
     */
    public void onItemDrag(InventoryDragEvent event, Player player) {
    }

    /**
     * Method called on ui open event
     *
     * @param event  bukkit event
     * @param player who opens ui
     */
    public void onUIOpen(InventoryOpenEvent event, Player player) {
    }

    /**
     * Method called on ui close event
     *
     * @param event  bukkit event
     * @param player who closes ui
     */
    public void onUIClose(InventoryCloseEvent event, Player player) {
    }

    /**
     * Create bukkit inventory
     *
     * @param title inventory's title
     * @param size  inventory's size
     */
    protected void createInventory(String title, int size) {
        int tmpSize = getNearestMultiple(size, 9);
        if (tmpSize < 9 || tmpSize > 54) tmpSize = 54;
        this.title = title;
        this.inventory = Bukkit.createInventory(null, tmpSize, color(title));
    }


    /**
     * Fill inventory's borders
     *
     * @param itemStack filler
     * @return list of ui buttons
     */
    protected List<UIButton> fillBorders(ItemStack itemStack) {
        return Arrays.stream(borderSlots[(inventory.getSize() / 9) - 1])
                .mapToObj(slot -> addItem(slot, itemStack))
                .collect(Collectors.toList());
    }

    /**
     * Fill inventory's borders
     *
     * @param builder filler
     * @return list of ui buttons
     */
    protected List<UIButton> fillBorders(ItemBuilder builder) {
        return fillBorders(builder.toItemStack());
    }

    /**
     * Fill inventory
     *
     * @param itemStack filler
     * @return list of ui buttons
     */
    protected List<UIButton> fillInside(ItemStack itemStack) {
        return Arrays.stream(insideSlots[(inventory.getSize() / 9) - 1])
                .mapToObj(slot -> addItem(slot, itemStack))
                .collect(Collectors.toList());
    }

    /**
     * Fill inventory
     *
     * @param itemBuilder filler
     * @return list of ui buttons
     */
    protected List<UIButton> fillInside(ItemBuilder itemBuilder) {
        return fillInside(itemBuilder.toItemStack());
    }

    /**
     * Fill empty spaces with fillers
     *
     * @param itemStack filler
     * @return list of ui buttons
     */
    protected List<UIButton> fillEmptySpaces(ItemStack itemStack) {
        List<UIButton> btns = new ArrayList<>();
        for (int i = 0; i < inventory.getSize(); i++)
            if (inventory.getItem(i) == null || Objects.requireNonNull(inventory.getItem(i)).getType().equals(Material.AIR))
                btns.add(addItem(i, itemStack));
        return btns;
    }

    /**
     * Fill empty spaces with fillers
     *
     * @param builder filler
     * @return list of ui buttons
     */
    protected List<UIButton> fillEmptySpaces(ItemBuilder builder) {
        return fillEmptySpaces(builder.toItemStack());
    }

    /**
     * Add ItemBuilder to ui
     *
     * @param slots       where the button will be
     * @param itemBuilder itemBuilder
     * @return UI Button
     */
    public List<UIButton> addItems(List<Integer> slots, ItemBuilder itemBuilder) {
        return addItems(slots, itemBuilder.toItemStack());
    }

    /**
     * Add ItemStack to ui
     *
     * @param slots     where the button will be
     * @param itemStack button's icon
     * @return button
     */
    public List<UIButton> addItems(List<Integer> slots, ItemStack itemStack) {
        return slots.stream()
                .map(slot -> addItem(slot, itemStack))
                .collect(Collectors.toList());
    }

    /**
     * Add ItemBuilder to ui
     *
     * @param slot        where the button will be
     * @param itemBuilder itemBuilder
     * @return UI Button
     */
    public UIButton addItem(int slot, ItemBuilder itemBuilder) {
        return addItem(slot, itemBuilder.toItemStack());
    }

    /**
     * Add ItemStack to ui
     *
     * @param slot      where the button will be
     * @param itemStack button's icon
     * @return button
     */
    public UIButton addItem(int slot, ItemStack itemStack) {
        getButton(slot).ifPresent(buttons::remove);
        UIButton button = new UIButton(slot, itemStack);
        buttons.add(button);
        inventory.setItem(slot, itemStack);
        return button;
    }

    /**
     * Get ui's button by slot
     *
     * @param slot button's slot
     * @return optional of UIButton
     */
    public Optional<UIButton> getButton(int slot) {
        return buttons.stream()
                .filter(b -> b.getSlot() == slot)
                .findFirst();
    }
}
