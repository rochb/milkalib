package me.sharkz.milkalib.spigot.commands.adapters;

import me.sharkz.milkalib.core.adapters.MilkaAdapter;
import org.bukkit.Material;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class MaterialAdapter implements MilkaAdapter<Material> {

    @Override
    public Optional<Material> parse(String data) {
        return Arrays.stream(Material.values()).filter(material -> material.name().equalsIgnoreCase(data)).findFirst();
    }

    @Override
    public Collection<String> getSuggestions() {
        return Arrays.stream(Material.values()).map(Enum::name).collect(Collectors.toList());
    }

}
