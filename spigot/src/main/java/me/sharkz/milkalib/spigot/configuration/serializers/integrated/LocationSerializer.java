package me.sharkz.milkalib.spigot.configuration.serializers.integrated;

import lombok.NonNull;
import me.sharkz.milkalib.spigot.configuration.serializers.YMLSerializer;
import me.sharkz.milkalib.spigot.exceptions.YMLDeserializationException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

import java.util.Objects;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LocationSerializer implements YMLSerializer<Location> {


    @Override
    public Optional<Location> deserialize(@NonNull ConfigurationSection section) throws YMLDeserializationException {
        if (!section.contains("x")
                || !section.contains("y")
                || !section.contains("z")
                || !section.isString("world")) throw new YMLDeserializationException("Missing location part!");
        double x = getDoubleOrInt(section, "x");
        double y = getDoubleOrInt(section, "y");
        double z = getDoubleOrInt(section, "z");

        float yaw = 0;
        if (section.isDouble("yaw"))
            yaw = (float) section.getDouble("yaw");

        float pitch = 0;
        if (section.isDouble("pitch"))
            pitch = (float) section.getDouble("pitch");

        String worldName = section.getString("world", "world");
        World world = Bukkit.getWorld(worldName);
        if (Objects.isNull(world)) throw new YMLDeserializationException("World " + worldName + " doesn't exists!");

        return Optional.of(new Location(world, x, y, z, yaw, pitch));
    }

    @Override
    public void serialize(ConfigurationSection section, @NonNull Location object) {
        section.set("x", object.getX());
        section.set("y", object.getY());
        section.set("z", object.getZ());
        if (object.getYaw() != 0) section.set("yaw", object.getYaw());
        if (object.getPitch() != 0) section.set("pitch", object.getPitch());
        section.set("world", Objects.requireNonNull(object.getWorld()).getName());
    }

    private double getDoubleOrInt(ConfigurationSection section, String path) {
        if (section.isInt(path)) return section.getInt(path);
        else if (section.isDouble(path)) return section.getDouble(path);
        return 0;
    }
}
