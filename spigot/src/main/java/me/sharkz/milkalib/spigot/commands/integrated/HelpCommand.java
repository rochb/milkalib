package me.sharkz.milkalib.spigot.commands.integrated;

import me.sharkz.milkalib.core.commands.CommandResult;
import me.sharkz.milkalib.core.translations.TranslationManager;
import me.sharkz.milkalib.core.utils.Pagination;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.commands.SpigotCommand;
import me.sharkz.milkalib.spigot.translations.HelpEn;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HelpCommand extends SpigotCommand {

    private final SpigotCommand command;
    private final Pagination<SpigotCommand> pagination = new Pagination<>();

    public HelpCommand(SpigotPlugin plugin, SpigotCommand command) {
        super(plugin, "help", "?");
        this.command = command;
        registerTranslation();
        setDescription(translate(HelpEn.HELP_COMMAND_DESCRIPTION).get(0));

        addOptionalArgument("page", Integer.class);
    }

    public HelpCommand(SpigotPlugin plugin) {
        super(plugin, "help", "?");
        this.command = null;
        registerTranslation();
        setDescription(translate(HelpEn.HELP_COMMAND_DESCRIPTION).get(0));

        addOptionalArgument("page", Integer.class);
    }

    @Override
    protected CommandResult execute(CommandSender sender, Object[] args) {
        int page = getArgument(args, Integer.class).orElse(1);
        List<SpigotCommand> commands = new LinkedList<>();
        if (Objects.isNull(command))
            commands.addAll(getSpigotPlugin().getCommandsManager()
                    .getCommands()
                    .stream()
                    .filter(command1 -> Objects.isNull(command1.getParent()))
                    .filter(command1 -> Objects.isNull(command1.getPermission()) || sender.hasPermission(command1.getPermission()))
                    .collect(Collectors.toList()));
        else
            commands.addAll(command.getSubCommands().stream()
                    .filter(milkaCommand -> Objects.isNull(milkaCommand.getPermission()) || sender.hasPermission(milkaCommand.getPermission()))
                    .map(milkaCommand -> (SpigotCommand) milkaCommand)
                    .collect(Collectors.toList()));
        List<SpigotCommand> cmds = new ArrayList<>();
        commands.forEach(command1 -> {
            cmds.add(command1);
            cmds.addAll(command1.getSubCommands().stream().map(milkaCommand -> (SpigotCommand) milkaCommand).collect(Collectors.toList()));
        });
        int maxPage = getMaxPage(cmds, 5);
        if (page > maxPage || page <= 0) {
            message(sender, HelpEn.HELP_COMMAND_UNKNOWN_PAGE);
            return CommandResult.SUCCESS;
        }
        List<SpigotCommand> toShow = pagination.paginate(cmds, 5, page);
        message(sender, HelpEn.HELP_MENU_HEADER, "name", getPlugin().getName());
        toShow.forEach(cmd -> message(sender, HelpEn.HELP_MENU_FORMAT, "command", "/" + cmd.getSyntax(), "description", cmd.getDescription() == null ? "?" : cmd.getDescription()));
        if (maxPage > 1)
            message(sender, HelpEn.HELP_MENU_PAGINATED_FOOTER, "name", getPlugin().getName(), "page", page, "maxpage", maxPage);
        else message(sender, HelpEn.HELP_MENU_FOOTER, "name", getPlugin().getName());
        return CommandResult.SUCCESS;
    }

    private void registerTranslation() {
        TranslationManager manager = getSpigotPlugin().getTranslationManager();
        if (manager.isRegistered(HelpEn.class)) return;
        manager.register(HelpEn.class);
    }
}
