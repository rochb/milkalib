package me.sharkz.milkalib.spigot.commands.integrated;

import me.sharkz.milkalib.core.commands.CommandResult;
import me.sharkz.milkalib.core.translations.integrated.CommandsEn;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.commands.SpigotCommand;
import org.bukkit.command.CommandSender;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ReloadCommand extends SpigotCommand {

    public ReloadCommand(SpigotPlugin plugin, String permission) {
        super(plugin, "reload", "rl");
        this.setPermission(permission);

        setDescription("Reload plugin configuration.");
    }

    @Override
    protected CommandResult execute(CommandSender sender, Object[] args) {
        getSpigotPlugin().onReload();
        message(sender, CommandsEn.PLUGIN_RELOADED_SUCCESSFULLY);
        return CommandResult.SUCCESS;
    }
}
