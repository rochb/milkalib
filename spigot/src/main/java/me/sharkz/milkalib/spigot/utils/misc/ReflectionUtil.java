package me.sharkz.milkalib.spigot.utils.misc;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ReflectionUtil {

    /**
     * Checks if the given class exists
     * @param className the name of the class
     * @return true if exists, false otherwise.
     */
    public static boolean hasClass(String className){
        try{
            Class.forName(className);
            return true;
        }catch (ClassNotFoundException e){
            return false;
        }
    }
}
