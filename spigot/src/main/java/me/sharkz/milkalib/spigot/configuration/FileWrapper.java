package me.sharkz.milkalib.spigot.configuration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.sharkz.milkalib.core.configurations.wrapper.YamlFileLoadException;
import me.sharkz.milkalib.core.configurations.wrapper.YmlFileWrapper;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Objects;

/**
 * Wrapper class that manages a Yaml configuration file stored on the disk.
 *
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
@Getter
public class FileWrapper implements YmlFileWrapper {

    /**
     * Java file object representation
     */
    private final File file;

    /**
     * URL of a default resource to use when initializing the file
     */
    private final URL defaultResource;

    /**
     * Configuration object used to retrieve and format the config file
     */
    protected FileConfiguration configuration;

    /**
     * Constructs a new configuration file wrapper without default resource.
     *
     * @param file Java file object
     */
    public FileWrapper(File file) {
        this(file, null);
    }

    /**
     * Constructs a new configuration file wrapper from file name
     *
     * @param plugin   spigot plugin
     * @param fileName file's name
     */
    public FileWrapper(SpigotPlugin plugin, String fileName) {
        if (!fileName.endsWith(".yml")) fileName += ".yml";
        this.file = new File(plugin.getDataFolder(), fileName);
        this.defaultResource = plugin.getClass().getClassLoader().getResource(fileName);
    }

    /**
     * Retrieves the configuration object.
     *
     * @return configuration object
     */
    public Configuration get() {
        if (this.configuration == null)
            throw new NullPointerException("configuration object of the file is null");
        return this.configuration;
    }

    /**
     * Loads the configuration file from the disk.
     * Create the file on the disk with the default resource if provided.
     *
     * @throws YamlFileLoadException thrown if the configuration file cannot be loaded
     */
    @Override
    public void load() throws YamlFileLoadException {
        configuration = createConfiguration(file);
        if (Objects.isNull(defaultResource)) return;

        try {
            if (!file.isFile() || !file.exists()) Files.copy(getDefaultResourceStream(), file.toPath());

            configuration.setDefaults(YamlConfiguration.loadConfiguration(
                    new InputStreamReader(getDefaultResourceStream(),
                            StandardCharsets.UTF_8)
            ));

            configuration = createConfiguration(file);
        } catch (IOException e) {
            throw new YamlFileLoadException("Cannot copy the default resource on the disk", e);
        }
    }

    /**
     * Saves the configuration file on the disk.
     *
     * @throws IOException thrown if the configuration file cannot be saved
     */
    @Override
    public void save() throws IOException {
        if (Objects.nonNull(configuration)) configuration.save(file);
    }

    @Override
    public boolean exists() {
        return file.exists();
    }

    /**
     * Retrieves and creates the configuration object from a file.
     *
     * @param file file object
     * @return created configuration instance from the file data
     */
    protected FileConfiguration createConfiguration(File file) {
        return YamlConfiguration.loadConfiguration(file);
    }

    /**
     * Creates a stream with the content of the default resource.
     *
     * @return input stream with the resource content
     */
    @Override
    public InputStream getDefaultResourceStream() throws IOException {
        URLConnection connection = defaultResource.openConnection();
        connection.setUseCaches(false);
        return connection.getInputStream();
    }


}
