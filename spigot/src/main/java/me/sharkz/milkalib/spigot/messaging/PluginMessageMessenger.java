package me.sharkz.milkalib.spigot.messaging;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import me.sharkz.milkalib.messaging.IncomingMessageConsumer;
import me.sharkz.milkalib.messaging.Messenger;
import me.sharkz.milkalib.messaging.message.OutgoingMessage;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collection;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@AllArgsConstructor
public class PluginMessageMessenger implements Messenger, PluginMessageListener {

    private final SpigotPlugin plugin;
    private final IncomingMessageConsumer consumer;

    private String channel;

    public void init(String channel) {
        this.channel = channel;
        plugin.getServer().getMessenger().registerOutgoingPluginChannel(plugin, channel);
        plugin.getServer().getMessenger().registerIncomingPluginChannel(plugin, channel, this);
    }

    @Override
    public void close() {
        plugin.getServer().getMessenger().unregisterIncomingPluginChannel(plugin, channel);
        plugin.getServer().getMessenger().unregisterOutgoingPluginChannel(plugin, channel);
    }

    @Override
    public void sendOutgoingMessage(@NonNull OutgoingMessage outgoingMessage) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(outgoingMessage.asEncodedString());
        byte[] data = out.toByteArray();

        new BukkitRunnable() {
            @Override
            public void run() {
                Collection<? extends Player> players = PluginMessageMessenger.this.plugin.getServer().getOnlinePlayers();
                Player p = Iterables.getFirst(players, null);
                if (p == null) return;
                p.sendPluginMessage(PluginMessageMessenger.this.plugin, channel, data);
                cancel();
            }
        }.runTaskTimer(this.plugin, 1L, 100L);
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals(this.channel))
            return;

        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String msg = in.readUTF();

        this.consumer.consumeIncomingMessageAsString(msg);
    }
}
