package me.sharkz.milkalib.spigot.listeners;

import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.ui.UIManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class UIListener extends MilkaListener {

    private final UIManager manager;

    public UIListener(SpigotPlugin plugin) {
        super(plugin);
        this.manager = plugin.getUiManager();
    }

    @EventHandler
    public void onUIClose(InventoryCloseEvent e) {
        if (!(e.getPlayer() instanceof Player)) return;
        Player player = (Player) e.getPlayer();
        manager.getPlayersManager()
                .getOpenInventory(player)
                .filter(milkaUI -> milkaUI.getInventory().equals(e.getInventory()))
                .ifPresent(milkaUI -> manager.handleUIClose(e, player, milkaUI));
    }

    @EventHandler
    public void onUIClick(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player)
                || Objects.isNull(e.getClickedInventory()))
            return;
        Player player = (Player) e.getWhoClicked();
        manager.getPlayersManager()
                .getOpenInventory(player)
                .filter(milkaUI -> milkaUI.getInventory().equals(e.getClickedInventory()) || milkaUI.getInventory().equals(e.getInventory()))
                .ifPresent(milkaUI -> {
                    if (!milkaUI.getInventory().equals(e.getClickedInventory())) return;
                    if (e.getClick().equals(ClickType.DOUBLE_CLICK)
                            || e.isShiftClick()
                            || !milkaUI.getAllowedClickSlots().contains(e.getRawSlot()))
                        e.setCancelled(true);
                    milkaUI.getButton(e.getRawSlot()).ifPresent(uiButton -> uiButton.handleClick(e));
                });
    }

    @EventHandler
    public void onUIDrag(InventoryDragEvent e) {
        if (!(e.getWhoClicked() instanceof Player)
                || e.isCancelled()) return;
        Player player = (Player) e.getWhoClicked();
        manager.getPlayersManager()
                .getOpenInventory(player)
                .filter(milkaUI -> milkaUI.getInventory().equals(e.getInventory()))
                .ifPresent(milkaUI -> {
                    if (milkaUI.getAllowedClickSlots().stream().noneMatch(integer -> e.getRawSlots().contains(integer)))
                        e.setCancelled(true);
                    else
                        milkaUI.onItemDrag(e, player);
                });
    }

}
