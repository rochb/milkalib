package me.sharkz.milkalib.spigot.commands.adapters;

import me.sharkz.milkalib.core.adapters.MilkaAdapter;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class WorldAdapter implements MilkaAdapter<World> {

    @Override
    public Optional<World> parse(String data) {
        return Optional.ofNullable(Bukkit.getWorld(data));
    }

    @Override
    public Collection<String> getSuggestions() {
        return Bukkit.getWorlds().stream().map(World::getName).collect(Collectors.toList());
    }
}
