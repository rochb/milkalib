package me.sharkz.milkalib.spigot.commands.adapters;

import me.sharkz.milkalib.core.adapters.MilkaAdapter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class OfflinePlayerAdapter implements MilkaAdapter<OfflinePlayer> {

    @Override
    public Optional<OfflinePlayer> parse(String data) {
        try {
            OfflinePlayer player = Bukkit.getOfflinePlayer(data);
            return Optional.of(player);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public Collection<String> getSuggestions() {
        return Arrays.stream(Bukkit.getOfflinePlayers()).map(OfflinePlayer::getName).collect(Collectors.toList());
    }
}
