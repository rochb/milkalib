package me.sharkz.milkalib.spigot.configuration.serializers;

import lombok.NonNull;
import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.objects.ObjectManager;
import me.sharkz.milkalib.spigot.exceptions.YMLDeserializationException;
import me.sharkz.milkalib.spigot.exceptions.YMLSerializationException;
import org.bukkit.configuration.ConfigurationSection;

import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SerializersManager extends ObjectManager<YMLSerializer<?>> {

    public SerializersManager(MilkaPlugin plugin) {
        super(plugin);
    }

    /**
     * Get serializer by it's primitive class
     *
     * @param primitiveClass serializer's primitive class
     * @return optional of serializer instance
     */
    public Optional<YMLSerializer<?>> getByPrimitiveClass(Class<?> primitiveClass) {
        return getObjects().stream().filter(ymlSerializer -> getGenericClass(ymlSerializer).equals(primitiveClass)).findFirst();
    }

    /**
     * Deserialize object from primitive class from configuration section.
     * <p>
     * {@link this.getPrimitiveClass(Class)}
     *
     * @param section        object's location
     * @param primitiveClass object type
     * @param <T>            object type
     * @return optional of object instance
     */
    public <T> Optional<T> deserialize(@NonNull ConfigurationSection section, Class<?> primitiveClass) {
        return getByPrimitiveClass(primitiveClass)
                .flatMap(serializer -> {
                    try {
                        return (Optional<T>) serializer.deserialize(section);
                    } catch (YMLDeserializationException e) {
                        e.printStackTrace();
                        return Optional.empty();
                    }
                });
    }

    /**
     * Deserialize objects list from primitive class from configuration section.
     * <p>
     * {@link this.getPrimitiveClass(Class)}
     *
     * @param section        objects' location
     * @param primitiveClass object type
     * @param <T>            object type
     * @return objects list
     */
    public <T> List<T> deserializeList(@NonNull ConfigurationSection section, Class<?> primitiveClass) {
        return (List<T>) getByPrimitiveClass(primitiveClass)
                .map(serializer -> serializer.deserializeList(section))
                .orElse(new ArrayList<>());
    }

    /**
     * Serialize object to configuration section
     * <p>
     * {@link this.getPrimitiveClass(Class)}
     *
     * @param section where to serialize object
     * @param object  targeted object
     * @param <T>     object primitive type
     */
    public <T> void serialize(@NonNull ConfigurationSection section, T object) {
        getByPrimitiveClass(object.getClass()).ifPresent(serializer -> {
            try {
                ((YMLSerializer<T>) serializer).serialize(section, object);
            } catch (YMLSerializationException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Register serializer by it's class & constructor's arguments
     *
     * @param serializerClass      serializer's class
     * @param constructorArguments constructor's arguments
     */
    public void register(Class<? extends YMLSerializer<?>> serializerClass, Object... constructorArguments) {
        try {
            Constructor<? extends YMLSerializer<?>> constructor = serializerClass.getDeclaredConstructor(Arrays.stream(constructorArguments).map(Object::getClass).collect(Collectors.toList()).toArray(new Class[]{}));
            YMLSerializer<?> serializer = constructor.newInstance(constructorArguments);
            add(serializer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Register serializer by it's class & constructor's arguments
     *
     * @param serializerClass      serializer's class
     * @param types                constructor's arguments' types
     * @param constructorArguments constructor's arguments
     */
    public void register(Class<? extends YMLSerializer<?>> serializerClass, Class<?>[] types, Object... constructorArguments) {
        try {
            Constructor<? extends YMLSerializer<?>> constructor = serializerClass.getDeclaredConstructor(types);
            YMLSerializer<?> serializer = constructor.newInstance(constructorArguments);
            add(serializer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Register multiples serializers without constructor arguments.
     *
     * @param serializerClasses serializers classes
     */
    public void register(Class<? extends YMLSerializer<?>>... serializerClasses) {
        for (Class<? extends YMLSerializer<?>> serializerClass : serializerClasses) {
            try {
                YMLSerializer<?> serializer = serializerClass.newInstance();
                add(serializer);
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private <T> Class<T> getGenericClass(YMLSerializer<?> serializer) {
        return (Class<T>) ((ParameterizedType) serializer.getClass().getGenericInterfaces()[0]).getActualTypeArguments()[0];
    }

}
