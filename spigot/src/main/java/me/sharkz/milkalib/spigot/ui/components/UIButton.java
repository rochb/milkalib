package me.sharkz.milkalib.spigot.ui.components;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
@Getter
public class UIButton {

    private final int slot;
    private final ItemStack itemStack;
    private Consumer<InventoryClickEvent> clickAction;
    private Consumer<InventoryClickEvent> rightClickAction;
    private Consumer<InventoryClickEvent> leftClickAction;
    private Consumer<InventoryClickEvent> middleClickAction;
    private Consumer<InventoryClickEvent> shiftClickAction;
    private Consumer<InventoryClickEvent> keyboardClickAction;

    /**
     * Handle inventory click event
     *
     * @param event bukkit inventory click event
     */
    public void handleClick(InventoryClickEvent event) {
        ClickType type = event.getClick();
        if (Objects.nonNull(clickAction)) clickAction.accept(event);
        if (type.isRightClick() & Objects.nonNull(rightClickAction)) rightClickAction.accept(event);
        if (type.isLeftClick() & Objects.nonNull(leftClickAction)) leftClickAction.accept(event);
        if (type.isCreativeAction() & Objects.nonNull(middleClickAction)) middleClickAction.accept(event);
        if (type.isShiftClick() && Objects.nonNull(shiftClickAction)) shiftClickAction.accept(event);
        if (type.isKeyboardClick() && Objects.nonNull(keyboardClickAction)) keyboardClickAction.accept(event);
    }

    public UIButton setClickAction(Consumer<InventoryClickEvent> clickAction) {
        this.clickAction = clickAction;
        return this;
    }


    public UIButton setRightClickAction(Consumer<InventoryClickEvent> rightClickAction) {
        this.rightClickAction = rightClickAction;
        return this;
    }


    public UIButton setLeftClickAction(Consumer<InventoryClickEvent> leftClickAction) {
        this.leftClickAction = leftClickAction;
        return this;
    }


    public UIButton setMiddleClickAction(Consumer<InventoryClickEvent> middleClickAction) {
        this.middleClickAction = middleClickAction;
        return this;
    }


    public UIButton setShiftClickAction(Consumer<InventoryClickEvent> shiftClickAction) {
        this.shiftClickAction = shiftClickAction;
        return this;
    }


    public UIButton setKeyboardClickAction(Consumer<InventoryClickEvent> keyboardClickAction) {
        this.keyboardClickAction = keyboardClickAction;
        return this;
    }

}
