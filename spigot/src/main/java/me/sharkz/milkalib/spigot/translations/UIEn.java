package me.sharkz.milkalib.spigot.translations;

import me.sharkz.milkalib.core.translations.Translation;
import me.sharkz.milkalib.core.translations.TranslationManager;
import me.sharkz.milkalib.core.translations.TranslationPack;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum UIEn implements TranslationPack {
    UI_ERROR("&c&l!! &eAn error occurred while opening this menu.", "&b&l!! &7Please contact an administrator.");

    private final List<String> content;
    private TranslationManager manager;

    UIEn(String... content) {
        this.content = Arrays.asList(content);
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), this.content);
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(UIEn::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(TranslationManager manager) {
        this.manager = manager;
    }

    @Override
    public TranslationManager getManager() {
        return this.manager;
    }

    public List<String> toList() {
        return this.get().translate();
    }
}
