package me.sharkz.milkalib.spigot.ui;

import lombok.Getter;
import lombok.Setter;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.ui.components.UIButton;
import me.sharkz.milkalib.spigot.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Roch Blondiaux
 */
@Getter
@Setter
public abstract class PaginatedUI extends MilkaUI {

    private final List<ItemStack> itemStacks;
    private final List<UIButton> itemButtons = new ArrayList<>();
    private ItemStack nextArrow;
    private ItemStack previousArrow;

    public PaginatedUI(SpigotPlugin plugin, List<ItemStack> itemStacks) {
        super(plugin);
        this.itemStacks = itemStacks;
        this.nextArrow = new ItemBuilder("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzg2MTg1YjFkNTE5YWRlNTg1ZjE4NGMzNGYzZjNlMjBiYjY0MWRlYjg3OWU4MTM3OGU0ZWFmMjA5Mjg3In19fQ==")
                .setName("&f&lNext")
                .toItemStack();
        this.previousArrow = new ItemBuilder("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWQ3M2NmNjZkMzFiODNjZDhiODY0NGMxNTk1OGMxYjczYzhkOTczMjNiODAxMTcwYzFkODg2NGJiNmE4NDZkIn19fQ==")
                .setName("&f&lPrevious")
                .toItemStack();
    }

    public PaginatedUI(SpigotPlugin plugin, List<ItemStack> itemStacks, ItemStack nextArrow, ItemStack previousArrow) {
        super(plugin);
        this.itemStacks = itemStacks;
        this.nextArrow = nextArrow;
        this.previousArrow = previousArrow;
    }

    @Override
    public void open(Player player, Object[] objects) {
        int page = 1;
        if (objects != null
                && objects[0] instanceof Integer)
            page = (Integer) objects[0];
        boolean hasNext = page < getMaxPage(itemStacks, getInventory().getSize() - 9);
        boolean hasPrevious = page > 1;

        display(page);

        /* Bottom fillers */
        for (int slot = getInventory().getSize() - 9; slot < getInventory().getSize(); slot++)
            addItem(slot, new ItemBuilder(Material.WHITE_STAINED_GLASS_PANE).setName("&r"));

        /* Pagination buttons */
        int finalPage = page;
        if (hasNext)
            addItem(getInventory().getSize() - 1, nextArrow)
                    .setClickAction(event -> open(player, new Object[]{finalPage + 1}));
        if (hasPrevious)
            addItem(getInventory().getSize() - 1, previousArrow)
                    .setClickAction(event -> open(player, new Object[]{finalPage - 1}));
    }

    public void display(int page) {
        List<ItemStack> toDisplays = paginate(itemStacks, getInventory().getSize() - 9, page);
        itemButtons.clear();

        /* Items */
        AtomicInteger i = new AtomicInteger(0);
        toDisplays.forEach(itemStack -> itemButtons.add(addItem(i.getAndIncrement(), itemStack)));
    }
}
