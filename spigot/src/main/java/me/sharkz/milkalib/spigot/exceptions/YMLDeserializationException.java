package me.sharkz.milkalib.spigot.exceptions;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class YMLDeserializationException extends Exception {

    public YMLDeserializationException(String message) {
        super(message);
    }

}
