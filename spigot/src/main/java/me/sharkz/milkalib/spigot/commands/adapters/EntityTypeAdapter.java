package me.sharkz.milkalib.spigot.commands.adapters;

import me.sharkz.milkalib.core.adapters.MilkaAdapter;
import org.bukkit.entity.EntityType;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class EntityTypeAdapter implements MilkaAdapter<EntityType> {

    @Override
    public Optional<EntityType> parse(String data) {
        return Arrays.stream(EntityType.values()).filter(type -> type.name().equalsIgnoreCase(data)).findFirst();
    }

    @Override
    public Collection<String> getSuggestions() {
        return Arrays.stream(EntityType.values()).map(EntityType::name).collect(Collectors.toList());
    }
}
