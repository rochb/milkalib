package me.sharkz.milkalib.spigot.configuration.serializers.integrated;

import lombok.NonNull;
import me.sharkz.milkalib.spigot.configuration.serializers.YMLSerializer;
import me.sharkz.milkalib.spigot.exceptions.YMLDeserializationException;
import me.sharkz.milkalib.spigot.utils.misc.Cuboid;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

import java.util.Objects;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class CuboidSerializer implements YMLSerializer<Cuboid> {

    private static final LocationSerializer locationSerializer = new LocationSerializer();

    @Override
    public Optional<Cuboid> deserialize(@NonNull ConfigurationSection section) throws YMLDeserializationException {
        if (!section.isConfigurationSection("point1")
                || !section.isConfigurationSection("point2"))
            return Optional.empty();
        Optional<Location> point1 = locationSerializer.deserialize(Objects.requireNonNull(section.getConfigurationSection("point1")));
        Optional<Location> point2 = locationSerializer.deserialize(Objects.requireNonNull(section.getConfigurationSection("point2")));
        if (!point1.isPresent() || !point2.isPresent())
            throw new YMLDeserializationException("One of the cuboid's points is null!");
        return Optional.of(new Cuboid(point1.get(), point2.get()));
    }

    @Override
    public void serialize(ConfigurationSection section, @NonNull Cuboid object) {
        locationSerializer.serialize(section.createSection("point1"), object.getLowerNE());
        locationSerializer.serialize(section.createSection("point2"), object.getUpperSW());
    }
}
