package me.sharkz.milkalib.spigot.translations;

import me.sharkz.milkalib.core.translations.Translation;
import me.sharkz.milkalib.core.translations.TranslationManager;
import me.sharkz.milkalib.core.translations.TranslationPack;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum DialogEn implements TranslationPack {
    DIALOG_HOW_TO_CLOSE("&b&l!! &7Type &ccancel&7 to close the dialog."),
    DIALOG_CANCELLED("&a&l!! &7You have &ccancelled&7 the dialog!"),
    DIALOG_INVALID_INPUT("&e&l!! &7Invalid input! Try again."),
    ;

    private final String content;
    private TranslationManager manager;

    DialogEn(String content) {
        this.content = content;
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), Collections.singletonList(this.content));
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(DialogEn::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(TranslationManager manager) {
        this.manager = manager;
    }

    @Override
    public TranslationManager getManager() {
        return this.manager;
    }

    public List<String> toList() {
        return this.get().translate();
    }
}
