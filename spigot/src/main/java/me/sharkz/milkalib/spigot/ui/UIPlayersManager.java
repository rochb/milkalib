package me.sharkz.milkalib.spigot.ui;

import lombok.NonNull;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class UIPlayersManager {

    private final Map<Player, MilkaUI> players = new HashMap<>();

    /**
     * Get player current open inventory
     *
     * @param player targeted player
     * @return optional of milka ui instance
     */
    public Optional<MilkaUI> getOpenInventory(Player player) {
        return Optional.ofNullable(players.get(player));
    }

    /**
     * Check if player has an open inventory
     *
     * @param player targeted player
     * @return true if player has an open inventory
     */
    public boolean hasOpenInventory(Player player) {
        return players.containsKey(player);
    }

    /**
     * Add player to players map
     *
     * @param player targeted player
     * @param ui     opened inventory
     */
    public void add(Player player, MilkaUI ui) {
        close(player);
        players.put(player, ui);
    }

    /**
     * Remove player from players map
     *
     * @param player to remove
     */
    public void remove(Player player) {
        players.remove(player);
    }

    /**
     * Close player open inventory
     *
     * @param player targeted player
     */
    public void close(Player player) {
        if (!hasOpenInventory(player)) return;
        player.closeInventory();
        remove(player);
    }

    /**
     * Close all open inventories
     */
    public void closeAll() {
        players.keySet().forEach(HumanEntity::closeInventory);
        players.clear();
    }

    /**
     * Get milka ui by inventory
     *
     * @param inventory bukkit inventory
     * @return optional of milka ui
     */
    public Optional<MilkaUI> getByInventory(@NonNull Inventory inventory) {
        return players.values().stream().filter(milkaUI -> milkaUI.getInventory().equals(inventory)).findFirst();
    }
}
