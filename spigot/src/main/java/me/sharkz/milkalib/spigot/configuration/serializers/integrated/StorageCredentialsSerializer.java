package me.sharkz.milkalib.spigot.configuration.serializers.integrated;

import lombok.NonNull;
import me.sharkz.milkalib.core.storage.StorageCredentials;
import me.sharkz.milkalib.spigot.configuration.serializers.YMLSerializer;
import org.bukkit.configuration.ConfigurationSection;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class StorageCredentialsSerializer implements YMLSerializer<StorageCredentials> {

    @Override
    public Optional<StorageCredentials> deserialize(@NonNull ConfigurationSection section) {
        if (!section.isConfigurationSection("pool-settings")) return Optional.empty();
        String address = section.getString("host", "127.0.0.1");
        String database = section.getString("database");
        int port = 3306;
        if (section.isString("port")) port = Integer.parseInt(Objects.requireNonNull(section.getString("port")));
        else if (section.isInt("port")) port = section.getInt("port");

        String username = section.getString("username");
        String password = section.getString("password");

        ConfigurationSection poolSettings = section.getConfigurationSection("pool-settings");
        assert poolSettings != null;
        int maxPoolSize = poolSettings.getInt("maximum-pool-size");
        int minIdle = poolSettings.getInt("minimum-idle");
        int maxLifeTime = poolSettings.getInt("maximum-lifetime");
        int connectionTimeout = poolSettings.getInt("connection-timeout");
        int keepAliveTime = poolSettings.getInt("keep-alive-time");

        Map<String, String> properties = new HashMap<>();
        ConfigurationSection propertiesSection = section.getConfigurationSection("properties");
        if (Objects.nonNull(propertiesSection))
            properties = new HashMap<>(Objects.requireNonNull(propertiesSection)
                    .getKeys(true)
                    .stream()
                    .filter(propertiesSection::isString)
                    .collect(Collectors.toMap(key -> key, propertiesSection::getString)));

        return Optional.of(new StorageCredentials(address, database, port, username, password, maxPoolSize, minIdle, maxLifeTime, keepAliveTime, connectionTimeout, properties));
    }

    @Override
    public void serialize(ConfigurationSection section, @NonNull StorageCredentials credentials) {
        section.set("host", credentials.getAddress());
        section.set("port", credentials.getPort());
        section.set("database", credentials.getDatabase());
        section.set("username", credentials.getUsername());
        section.set("password", credentials.getPassword());

        ConfigurationSection poolSettings = section.createSection("pool-settings");
        poolSettings.set("maximum-pool-size", credentials.getMaxPoolSize());
        poolSettings.set("minimum-idle", credentials.getMinIdleConnections());
        poolSettings.set("maximum-lifetime", credentials.getMaxLifetime());
        poolSettings.set("connection-timeout", credentials.getConnectionTimeout());
        poolSettings.set("keep-alive-time", credentials.getKeepAliveTime());

        ConfigurationSection propertiesSection = section.createSection("properties");
        credentials.getProperties().forEach(propertiesSection::set);
    }

}
