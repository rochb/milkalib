package me.sharkz.milkalib.spigot.configuration;

import lombok.Getter;
import lombok.NonNull;
import me.sharkz.milkalib.core.configurations.MilkaConfigurationManager;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.configuration.serializers.SerializersManager;
import me.sharkz.milkalib.spigot.configuration.serializers.integrated.*;
import org.bukkit.configuration.ConfigurationSection;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class ConfigurationManager extends MilkaConfigurationManager<FileWrapper> {

    private final SerializersManager serializersManager;

    public ConfigurationManager(SpigotPlugin plugin) {
        super(plugin);
        this.serializersManager = new SerializersManager(plugin);
        this.serializersManager.register(LocationSerializer.class,
                CuboidSerializer.class,
                ItemStackSerializer.class,
                StorageCredentialsSerializer.class);
        this.serializersManager.register(MilkaStorageSerializer.class, new Class[]{SpigotPlugin.class}, plugin);
    }

    /**
     * Deserialize object from primitive class from configuration section.
     *
     * @param section        where to save
     * @param primitiveClass object type
     * @param <T>            object type
     * @return optional of object instance
     */
    public <T> Optional<T> deserialize(@NonNull ConfigurationSection section, Class<?> primitiveClass) {
        return serializersManager.deserialize(section, primitiveClass);
    }

    /**
     * Serialize object to configuration section
     *
     * @param section where to serialize object
     * @param object  targeted object
     * @param <T>     object primitive type
     */
    public <T> void serialize(@NonNull ConfigurationSection section, T object) {
        serializersManager.serialize(section, object);
    }

}
