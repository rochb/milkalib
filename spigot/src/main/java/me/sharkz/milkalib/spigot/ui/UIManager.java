package me.sharkz.milkalib.spigot.ui;

import lombok.Getter;
import me.sharkz.milkalib.core.objects.ObjectManager;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.translations.UIEn;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class UIManager extends ObjectManager<MilkaUI> {

    private final UIPlayersManager playersManager;
    private final List<UIConstructorInfo> constructorInfos = new ArrayList<>();

    public UIManager(SpigotPlugin plugin) {
        super(plugin);
        plugin.getTranslationManager().register(UIEn.class);
        this.playersManager = new UIPlayersManager();
    }

    /**
     * Open inventory to player
     *
     * @param player  targeted player
     * @param uiClass ui to open
     * @param objects objects to pass trough ui open method
     */
    public void open(Player player, Class<? extends MilkaUI> uiClass, Object... objects) {
        getByClass(uiClass).flatMap(milkaUI -> constructorInfos.stream()
                .filter(uiConstructorInfo -> uiConstructorInfo.getClazz().equals(uiClass))
                .findFirst())
                .ifPresent(uiConstructorInfo -> {
                    MilkaUI ui = null;
                    try {
                        Constructor<? extends MilkaUI> constructor = uiClass.getDeclaredConstructor(uiConstructorInfo.getClasses());
                        ui = constructor.newInstance(uiConstructorInfo.getObjects());
                        ui.open(player, objects);
                    } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                        color(UIEn.UI_ERROR).forEach(player::sendMessage);
                    }
                    playersManager.add(player, ui);
                    player.openInventory(Objects.requireNonNull(ui).getInventory());
                });
    }

    /**
     * Register MilkaUI by it's class
     *
     * @param uiClass              ui's class
     * @param constructorArguments ui's class constructor arguments
     */
    public void register(Class<? extends MilkaUI> uiClass, Object... constructorArguments) {
        try {
            Class<?>[] classes = Arrays.stream(constructorArguments).map(Object::getClass).collect(Collectors.toList()).toArray(new Class<?>[]{});
            Constructor<? extends MilkaUI> constructor = uiClass.getDeclaredConstructor(classes);
            MilkaUI ui = constructor.newInstance(constructorArguments);
            constructorInfos.add(new UIConstructorInfo(uiClass, classes, constructorArguments));
            add(ui);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void register(Class<? extends MilkaUI> uiClass, Class<?>[] classes, Object... constructorArg) {
        try {
            Constructor<? extends MilkaUI> constructor = uiClass.getDeclaredConstructor(classes);
            MilkaUI ui = constructor.newInstance(constructorArg);
            constructorInfos.add(new UIConstructorInfo(uiClass, classes, constructorArg));
            add(ui);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Unregister MilkaUI by it's class
     *
     * @param uiClass ui's class
     */
    public void unregister(Class<? extends MilkaUI> uiClass) {
        getObjects().removeIf(milkaUI -> milkaUI.getClass().equals(uiClass));
    }

    /**
     * Get MilkaUI by it's class
     *
     * @param uiClass ui's class
     * @return optional of ui instance
     */
    public Optional<MilkaUI> getByClass(Class<? extends MilkaUI> uiClass) {
        return getObjects().stream().filter(milkaUI -> milkaUI.getClass().equals(uiClass)).findFirst();
    }

    /**
     * Handle MilkaUI close event
     *
     * @param event  bukkit inventory close event
     * @param player player
     * @param ui     close ui
     */
    public void handleUIClose(InventoryCloseEvent event, Player player, MilkaUI ui) {
        ui.onUIClose(event, player);
        playersManager.remove(player);
    }

    /**
     * Close & delete all cached players open ui
     */
    public void unload() {
        playersManager.closeAll();
    }
}
