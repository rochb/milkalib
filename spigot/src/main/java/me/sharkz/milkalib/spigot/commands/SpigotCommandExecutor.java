package me.sharkz.milkalib.spigot.commands;

import me.sharkz.milkalib.core.adapters.AdaptersManager;
import me.sharkz.milkalib.core.commands.CommandResult;
import me.sharkz.milkalib.core.commands.MilkaCommand;
import me.sharkz.milkalib.core.commands.MilkaCommandExecutor;
import me.sharkz.milkalib.core.commands.SenderType;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SpigotCommandExecutor extends MilkaCommandExecutor implements CommandExecutor, TabExecutor {

    public SpigotCommandExecutor(SpigotPlugin plugin, SpigotCommandManager manager, AdaptersManager adaptersManager) {
        super(plugin, manager, adaptersManager);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        getManager()
                .getByKeyOrAliases(label)
                .ifPresent(ncmd -> {
                    if (!(ncmd instanceof SpigotCommand)) return;
                    CommandResult result = executeCommand((SpigotCommand) ncmd, sender, args, 0);
                    if (result.hasMessage())
                        colorAndFormat(result.getMessage(), "syntax", "/" + getCmdSyntax((SpigotCommand) ncmd, args, 0), "sender", capitalizeAllWords(ncmd.getAllowedSenderType().name().toLowerCase())).forEach(sender::sendMessage);
                });
        return true;
    }

    private String getCmdSyntax(SpigotCommand command, String[] args, int index) {
        if (args.length == 0
                || args.length - index == 0)
            return command.getSyntax();
        Optional<MilkaCommand> sub = command.getSubCommands()
                .stream()
                .filter(milkaCommand -> milkaCommand.getKey().equalsIgnoreCase(args[index]) || milkaCommand.getAliases().stream().anyMatch(s -> s.equalsIgnoreCase(args[index])))
                .findFirst();
        if (!sub.isPresent()) return command.getSyntax();
        return getCmdSyntax((SpigotCommand) sub.get(), args, index + 1);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        return getManager().getByKeyOrAliases(command.getName())
                .map(milkaCommand -> tabLimit(getTabSuggestions(milkaCommand, 0, args, sender::hasPermission), args[args.length - 1]))
                .orElse(new ArrayList<>());
    }

    private CommandResult executeCommand(SpigotCommand cmd, CommandSender sender, String[] args, int argIndex) {
        if (Objects.nonNull(cmd.getPermission()) && !sender.hasPermission(cmd.getPermission()))
            return CommandResult.FORBIDDEN;

        for (int i = argIndex; i < args.length; i++) {
            String argI = args[i];
            Optional<MilkaCommand> nCmd = cmd.getSubCommands()
                    .stream()
                    .filter(command -> command.getKey().equalsIgnoreCase(argI) || command.getAliases().stream().anyMatch(s -> s.equalsIgnoreCase(argI)))
                    .findFirst();
            if (nCmd.isPresent())
                return executeCommand((SpigotCommand) nCmd.get(), sender, args, i + 1);
        }

        if ((cmd.getAllowedSenderType().equals(SenderType.PLAYER) && !(sender instanceof Player))
                || (cmd.getAllowedSenderType().equals(SenderType.CONSOLE) && (sender instanceof Player)))
            return CommandResult.UNSUPPORTED_SENDER;

        Object[] nArgs = parseArgs(cmd, argIndex, args);
        CommandResult result = areArgsValid(cmd, nArgs, argIndex);
        if (!result.equals(CommandResult.SUCCESS)) return result;
        try {
            return cmd.execute(sender, nArgs);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.ERROR;
        }
    }
}
