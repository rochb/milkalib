package me.sharkz.milkalib.spigot.dialog;

import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.listeners.MilkaListener;
import me.sharkz.milkalib.spigot.translations.DialogEn;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class Dialog extends MilkaListener {

    private static boolean translationRegistered = false;

    private final Player player;
    private final List<String> text;
    private final DialogValidator validator;
    private final BiConsumer<Player, String> complete;
    private final Consumer<Player> cancel;
    private final boolean cancellable;

    public Dialog(SpigotPlugin plugin, Player player, List<String> text, DialogValidator validator, BiConsumer<Player, String> complete, Consumer<Player> cancel, boolean cancellable) {
        super(plugin);
        this.player = player;
        this.text = text;
        this.validator = validator;
        this.complete = complete;
        this.cancel = cancel;
        this.cancellable = cancellable;

        if (!translationRegistered) {
            plugin.getTranslationManager().register(DialogEn.class);
            translationRegistered = true;
        }
    }

    public void open() {
        player.closeInventory();

        if (cancellable)
            message(player, DialogEn.DIALOG_HOW_TO_CLOSE);

        text.forEach(s -> player.sendMessage(color(s)));
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        if (!e.getPlayer().equals(player)) return;
        e.setCancelled(true);
        String message = e.getMessage();
        runSync(() -> {
            if (message.equalsIgnoreCase("cancel") && cancellable) {
                cancel();
                return;
            }
            if (validator.isValid(message))
                close(message);
            else
                message(player, DialogEn.DIALOG_INVALID_INPUT);
        });
    }

    public void cancel() {
        if (Objects.nonNull(cancel)) cancel.accept(player);
        message(player, DialogEn.DIALOG_CANCELLED);
        end();
    }

    public void close(String input) {
        if (Objects.nonNull(complete)) complete.accept(player, input);
        end();
    }

    private void end() {
        HandlerList.unregisterAll(this);
    }


    public interface DialogValidator {

        boolean isValid(String input);

    }
}
