package me.sharkz.milkalib.spigot.listeners;

import lombok.Getter;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.utils.SpigotUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class MilkaListener extends SpigotUtils implements Listener {

    @Getter
    private boolean registered;

    public MilkaListener(SpigotPlugin plugin) {
        super(plugin);
        register();
    }

    protected void register() {
        if (registered) return;
        Bukkit.getServer().getPluginManager().registerEvents(this, getSpigotPlugin());
        registered = true;
    }

    protected void unregister() {
        if (!registered) return;
        HandlerList.unregisterAll(this);
        registered = false;
    }
}
