package me.sharkz.milkalib.spigot.translations;

import me.sharkz.milkalib.core.translations.Translation;
import me.sharkz.milkalib.core.translations.TranslationManager;
import me.sharkz.milkalib.core.translations.TranslationPack;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum HelpEn implements TranslationPack {
    HELP_MENU_HEADER("&f&m          &b&l %name% &f&m          "),
    HELP_MENU_FORMAT(" &b%command%  &f%description%"),
    HELP_MENU_FOOTER("&f&m          &b&l %name% &f&m          "),
    HELP_MENU_PAGINATED_FOOTER("&f&m                   &b&l %page%&7/&f%maxpage% &f&m                   "),
    HELP_COMMAND_DESCRIPTION("Displays help menu."),
    HELP_COMMAND_UNKNOWN_PAGE("&c&l!! &eThis page dosen't exists!"),
    ;

    private final String content;
    private TranslationManager manager;

    HelpEn(String content) {
        this.content = content;
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), Collections.singletonList(this.content));
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(HelpEn::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(TranslationManager manager) {
        this.manager = manager;
    }

    @Override
    public TranslationManager getManager() {
        return this.manager;
    }

    public List<String> toList() {
        return this.get().translate();
    }
}
