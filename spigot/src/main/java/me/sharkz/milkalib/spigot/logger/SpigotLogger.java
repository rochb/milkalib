package me.sharkz.milkalib.spigot.logger;

import lombok.Getter;
import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.logger.MilkaLogger;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class SpigotLogger implements MilkaLogger {

    private final SpigotPlugin plugin;
    private final String prefix;
    private boolean debug;

    public SpigotLogger(SpigotPlugin plugin) {
        this.plugin = plugin;
        this.prefix = plugin.getPrefix();
        this.debug = false;
    }

    @Override
    public void error(String message) {
        Bukkit.getConsoleSender().sendMessage(color("&c" + message));
    }

    @Override
    public void info(String message) {
        Bukkit.getConsoleSender().sendMessage(color("&f" + message));
    }

    @Override
    public void warn(String message) {
        Bukkit.getConsoleSender().sendMessage(color("&e" + message));
    }

    @Override
    public void success(String message) {
        Bukkit.getConsoleSender().sendMessage(color("&a" + message));
    }

    @Override
    public void debug(String message) {
        Bukkit.getConsoleSender().sendMessage(color("&7[&bDEBUG&7] " + message));
    }

    @Override
    public void error(String message, Throwable throwable) {
        Bukkit.getConsoleSender().sendMessage(color("&c" + message));
        Bukkit.getConsoleSender().sendMessage(color("&c" + throwable.getMessage()));
    }

    @Override
    public void warn(String message, Throwable throwable) {
        Bukkit.getConsoleSender().sendMessage(color("&e" + message));
        Bukkit.getConsoleSender().sendMessage(color("&c" + throwable.getMessage()));
    }

    @Override
    public boolean isDebugEnabled() {
        return debug;
    }

    @Override
    public void setDebugEnabled(boolean debug) {
        this.debug = debug;
    }

    private String color(String a) {
        return ChatColor.translateAlternateColorCodes('&', prefix + " " + a);
    }

    @Override
    public MilkaPlugin getPlugin() {
        return plugin;
    }
}
