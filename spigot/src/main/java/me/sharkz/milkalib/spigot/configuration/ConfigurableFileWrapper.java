package me.sharkz.milkalib.spigot.configuration;

import me.sharkz.milkalib.core.configurations.Configurable;
import me.sharkz.milkalib.core.configurations.IFileWrapper;
import me.sharkz.milkalib.core.configurations.wrapper.YamlFileLoadException;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.configuration.serializers.SerializersManager;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents a dynamically configurable
 * wrapper for a configuration file on the disk.
 *
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public abstract class ConfigurableFileWrapper extends FileWrapper implements IFileWrapper {

    private final SpigotPlugin plugin;

    /**
     * Constructs the configurable file wrapper.
     *
     * @param plugin spigot plugin
     * @param file   file object
     */
    public ConfigurableFileWrapper(SpigotPlugin plugin, File file) {
        super(file);
        this.plugin = plugin;
    }

    /**
     * Constructs the configurable file wrapper with a default resource.
     *
     * @param plugin          spigot plugin
     * @param file            file object
     * @param defaultResource default resource URL
     */
    public ConfigurableFileWrapper(SpigotPlugin plugin, File file, URL defaultResource) {
        super(file, defaultResource);
        this.plugin = plugin;
    }

    /**
     * Constructs a new configuration file wrapper from file name
     *
     * @param plugin   spigot plugin
     * @param plugin   spigot plugin
     * @param fileName file's name
     */
    public ConfigurableFileWrapper(SpigotPlugin plugin, String fileName) {
        super(plugin, fileName);
        this.plugin = plugin;
    }

    /**
     * Loads the configuration file from the disk.
     * Fills attributes of the instance with values from the file.
     *
     * @throws YamlFileLoadException thrown if the configuration file cannot be loaded
     */
    @Override
    public void load() throws YamlFileLoadException {
        super.load();
        SerializersManager serializersManager = plugin.getConfigurationManager().getSerializersManager();

        for (Field field : this.getClass().getDeclaredFields()) {
            Configurable conf = field.getAnnotation(Configurable.class);
            if (Objects.isNull(conf)) continue;

            String configKey = (conf.key().isEmpty()) ? field.getName() : conf.key();
            Object value = null;
            if (configuration.isConfigurationSection(configKey)) {
                ConfigurationSection section = configuration.getConfigurationSection(configKey);
                if (field.getType().equals(ConfigurationSection.class))
                    value = section;
                else if (field.getGenericType() instanceof ParameterizedType
                        && field.getType().equals(List.class)) {
                    ParameterizedType genericType = (ParameterizedType) field.getGenericType();
                    Class<?> fieldType = (Class<?>) genericType.getActualTypeArguments()[0];
                    value = serializersManager.deserializeList(section, fieldType);
                    if (Objects.isNull(value)) value = new ArrayList<>();
                } else
                    value = serializersManager
                            .deserialize(Objects.requireNonNull(section), field.getType())
                            .orElse(null);
            }
            if (Objects.isNull(value)) value = this.configuration.get(configKey);

            try {
                field.setAccessible(true);
                field.set(this, value);
                field.setAccessible(false);
            } catch (ReflectiveOperationException | IllegalArgumentException e) {
                String configName = getClass().getSimpleName().toLowerCase();
                throw new YamlFileLoadException(String.format(
                        "Cannot set the config value %s of key %s for %s",
                        value, configKey, configName
                ), e);
            }
        }
    }

}
