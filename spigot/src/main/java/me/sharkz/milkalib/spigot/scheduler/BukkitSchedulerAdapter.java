package me.sharkz.milkalib.spigot.scheduler;

import me.sharkz.milkalib.core.scheduler.AbstractJavaScheduler;
import me.sharkz.milkalib.core.scheduler.SchedulerAdapter;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BukkitSchedulerAdapter extends AbstractJavaScheduler implements SchedulerAdapter {

    private final SpigotPlugin plugin;
    private final Executor sync;
    private final BukkitScheduler scheduler;

    public BukkitSchedulerAdapter(SpigotPlugin plugin) {
        this.plugin = plugin;
        this.sync = r -> plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, r);
        this.scheduler = plugin.getServer().getScheduler();
    }

    @Override
    public Executor sync() {
        return this.sync;
    }

    @Override
    public void syncLater(Runnable task, long delay, TimeUnit unit) {
        scheduler.runTaskLater(plugin, task, unit.toSeconds(delay) * 20);
    }

    @Override
    public void syncRepeating(Runnable task, long interval, TimeUnit unit) {
        scheduler.runTaskTimer(plugin, task, 0, unit.toSeconds(interval) * 20);
    }

}