package me.sharkz.milkalib.spigot.utils;

import me.sharkz.milkalib.core.translations.TranslationPack;
import me.sharkz.milkalib.core.utils.MilkaUtils;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.ui.MilkaUI;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.Permission;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.Collection;
import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@SuppressWarnings("unused")
public class SpigotUtils extends MilkaUtils {

    public SpigotUtils(SpigotPlugin plugin) {
        super(plugin);
    }

    /**
     * Run synchronous task
     *
     * @param runnable to run
     * @return bukkit task
     */
    protected BukkitTask runSync(BukkitRunnable runnable) {
        return runnable.runTask(getSpigotPlugin());
    }

    /**
     * Run repeated synchronous task
     *
     * @param runnable to run
     * @param delay    before running
     * @param period   between each run
     * @return bukkit task
     */
    protected BukkitTask runRepeatedSync(BukkitRunnable runnable, long delay, long period) {
        return runnable.runTaskTimer(getSpigotPlugin(), delay, period);
    }

    /**
     * Run synchronous task later
     *
     * @param runnable to run
     * @param delay    before running
     * @return bukkit task
     */
    protected BukkitTask runSyncLater(BukkitRunnable runnable, long delay) {
        return runnable.runTaskLater(getSpigotPlugin(), delay);
    }

    /**
     * Run asynchronous task
     *
     * @param runnable to run
     * @return bukkit task
     */
    protected BukkitTask runAsync(BukkitRunnable runnable) {
        return runnable.runTaskAsynchronously(getSpigotPlugin());
    }

    /**
     * Run repeated asynchronous task
     *
     * @param runnable to run
     * @param delay    before running
     * @param period   between each run
     * @return bukkit task
     */
    protected BukkitTask runRepeatedAsync(BukkitRunnable runnable, long delay, long period) {
        return runnable.runTaskTimerAsynchronously(getSpigotPlugin(), delay, period);
    }

    /**
     * Run asynchronous task later
     *
     * @param runnable to run
     * @param delay    before running
     * @return bukkit task
     */
    protected BukkitTask runAsyncLater(BukkitRunnable runnable, long delay) {
        return runnable.runTaskLaterAsynchronously(getSpigotPlugin(), delay);
    }

    /**
     * Check if permissible has permission
     *
     * @param permissible permissible to check
     * @param permission  permission to have
     * @return true if's the case
     */
    protected boolean hasPermission(Permissible permissible, String permission) {
        return permissible.hasPermission(permission);
    }

    /**
     * Check if permissible has permission
     *
     * @param permissible permissible to check
     * @param permission  permission to have
     * @return true if's the case
     */
    protected boolean hasPermission(Permissible permissible, Permission permission) {
        return permissible.hasPermission(permission);
    }

    /**
     * Add permission to bukkit registry
     *
     * @param permission permission to register
     */
    protected void addPermission(Permission permission) {
        getSpigotPlugin().getServer().getPluginManager().addPermission(permission);
    }

    /**
     * Add permission to bukkit registry
     *
     * @param permission permission to register
     */
    protected void addPermission(String permission) {
        addPermission(new Permission(permission));
    }

    /**
     * Add permission to bukkit registry
     *
     * @param permission  permission to register
     * @param description permission's description
     */
    protected void addPermission(String permission, String description) {
        addPermission(new Permission(permission, description));
    }

    /**
     * Remove permission from bukkit registry
     *
     * @param permission permission to remove
     */
    protected void removePermission(String permission) {
        getSpigotPlugin().getServer().getPluginManager().removePermission(permission);
    }

    /**
     * Remove permission from bukkit registry
     *
     * @param permission permission to remove
     */
    protected void removePermission(Permission permission) {
        getSpigotPlugin().getServer().getPluginManager().removePermission(permission);
    }

    /**
     * Open ui to player
     *
     * @param player  targeted player
     * @param ui      ui to open
     * @param objects objects to pass trough ui open method
     */
    protected void openUI(Player player, Class<? extends MilkaUI> ui, Object... objects) {
        getSpigotPlugin().getUiManager().open(player, ui, objects);
    }

    /**
     * Send translated & colored message to command sender
     *
     * @param sender      targeted command sender
     * @param translation translation entry
     * @param <T>         translation class
     */
    protected <T extends TranslationPack> void message(CommandSender sender, T translation) {
        color(translation).forEach(sender::sendMessage);
    }

    /**
     * Send translated, formatted & colored message to command sender
     *
     * @param sender       targeted command sender
     * @param translation  translation entry
     * @param placeholders placeholders map
     * @param <T>          translation class
     */
    protected <T extends TranslationPack> void message(CommandSender sender, T translation, Map<String, String> placeholders) {
        colorAndFormat(translation, placeholders).forEach(sender::sendMessage);
    }

    /**
     * Send translated, formatted & colored message to command sender
     *
     * @param sender       targeted command sender
     * @param translation  translation entry
     * @param placeholders placeholders
     * @param <T>          translation class
     */
    protected <T extends TranslationPack> void message(CommandSender sender, T translation, Object... placeholders) {
        colorAndFormat(translation, placeholders).forEach(sender::sendMessage);
    }

    /**
     * Broadcast translated, formatted & colored message to command sender
     *
     * @param translation  translation entry
     * @param placeholders placeholders
     * @param <T>          translation class
     */
    protected <T extends TranslationPack> void broadcast(T translation, Object... placeholders) {
        colorAndFormat(translation, placeholders).forEach(Bukkit::broadcastMessage);
    }

    /**
     * Give or drop a single itemstack
     *
     * @param player    targeted player
     * @param itemStack itemstack to give
     */
    protected void giveOrDrop(Player player, ItemStack itemStack) {
        if (player.getInventory().firstEmpty() == -1)
            player.getWorld().dropItem(player.getLocation(), itemStack);
        else
            player.getInventory().addItem(itemStack);
    }

    /**
     * Give or drop multiples itemstacks
     *
     * @param player     targeted player
     * @param itemStacks itemstacks array to give
     */
    protected void giveOrDrop(Player player, ItemStack... itemStacks) {
        for (ItemStack itemStack : itemStacks)
            giveOrDrop(player, itemStack);
    }

    /**
     * Give or drop multiples itemstacks
     *
     * @param player     targeted player
     * @param itemStacks itemstacks collections to give
     */
    protected void giveOrDrop(Player player, Collection<ItemStack> itemStacks) {
        itemStacks.forEach(itemStack -> giveOrDrop(player, itemStack));
    }

    /**
     * Get text component from legacy text
     *
     * @param text legacy text
     * @return text component
     */
    protected TextComponent createTextComponent(String text) {
        return new TextComponent(TextComponent.fromLegacyText(color(text), ChatColor.WHITE));
    }

    /**
     * Get text component from translation
     *
     * @param translation translation
     * @return text component
     */
    protected <T extends TranslationPack> TextComponent createTextComponent(T translation) {
        return createTextComponent(translate(translation).get(0));
    }

    /**
     * Get text component from legacy text with placeholders
     *
     * @param text         legacy text
     * @param placeholders placeholders
     * @return formatted text component
     */
    protected TextComponent createTextComponent(String text, Object... placeholders) {
        return createTextComponent(format(text, placeholders));
    }

    /**
     * Get text component from translation with placeholders
     *
     * @param translation  translation
     * @param placeholders placeholders
     * @return text component
     */
    protected <T extends TranslationPack> TextComponent createTextComponent(T translation, Object... placeholders) {
        return createTextComponent(format(translation, placeholders).toArray(new String[]{})[0]);
    }

    /**
     * Set text component click action
     *
     * @param component target
     * @param event     click action
     * @return updated text component
     */
    protected TextComponent setClickAction(TextComponent component, ClickEvent event) {
        component.setClickEvent(event);
        return component;
    }

    /**
     * Set text component hover action
     *
     * @param component target
     * @param event     hover event
     * @return updated text component
     */
    protected TextComponent setHoverAction(TextComponent component, HoverEvent event) {
        component.setHoverEvent(event);
        return component;
    }

    /**
     * Send text component to player
     *
     * @param player    target
     * @param component content
     */
    protected void message(Player player, TextComponent component) {
        player.spigot().sendMessage(component);
    }

    /**
     * Get spigot plugin
     *
     * @return spigot plugin instance
     */
    protected SpigotPlugin getSpigotPlugin() {
        return (SpigotPlugin) getPlugin();
    }
}
