package me.sharkz.milkalib.spigot.configuration.serializers;

import lombok.NonNull;
import me.sharkz.milkalib.spigot.exceptions.YMLDeserializationException;
import me.sharkz.milkalib.spigot.exceptions.YMLSerializationException;
import org.bukkit.configuration.ConfigurationSection;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface YMLSerializer<T> {

    /**
     * Deserialize object from configuration section
     *
     * @param section object's location
     * @return optional of deserialized object
     */
    Optional<T> deserialize(@NonNull ConfigurationSection section) throws YMLDeserializationException;

    /**
     * Deserialize list of objects from configuration section
     *
     * @param section objects' location
     * @return list of deserialized objects
     */
    default List<T> deserializeList(@NonNull ConfigurationSection section) {
        return section.getKeys(true)
                .stream()
                .map(section::getConfigurationSection)
                .filter(Objects::nonNull)
                .map(section1 -> {
                    try {
                        return deserialize(section1);
                    } catch (YMLDeserializationException e) {
                        e.printStackTrace();
                    }
                    return (Optional<T>) Optional.empty();
                })
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    /**
     * Serialize object to configuration section
     *
     * @param section where to save
     * @param object  object to serialize
     */
    void serialize(ConfigurationSection section, @NonNull T object) throws YMLSerializationException;
}
