package me.sharkz.milkalib.spigot;

import lombok.Getter;
import me.sharkz.milkalib.core.MilkaPlugin;
import me.sharkz.milkalib.core.adapters.AdaptersManager;
import me.sharkz.milkalib.core.adapters.integrated.BooleanAdapter;
import me.sharkz.milkalib.core.adapters.integrated.DoubleAdapter;
import me.sharkz.milkalib.core.adapters.integrated.IntegerAdapter;
import me.sharkz.milkalib.core.adapters.integrated.StringAdapter;
import me.sharkz.milkalib.core.dependencies.Dependency;
import me.sharkz.milkalib.core.dependencies.DependencyManager;
import me.sharkz.milkalib.core.dependencies.classloader.PluginClassLoader;
import me.sharkz.milkalib.core.dependencies.classloader.ReflectionClassLoader;
import me.sharkz.milkalib.core.exceptions.CommandRegistrationException;
import me.sharkz.milkalib.core.exceptions.CommandUnregistrationException;
import me.sharkz.milkalib.core.hooks.PluginHooker;
import me.sharkz.milkalib.core.translations.TranslationManager;
import me.sharkz.milkalib.core.translations.TranslationPack;
import me.sharkz.milkalib.spigot.commands.SpigotCommand;
import me.sharkz.milkalib.spigot.commands.SpigotCommandManager;
import me.sharkz.milkalib.spigot.commands.adapters.*;
import me.sharkz.milkalib.spigot.configuration.ConfigurationManager;
import me.sharkz.milkalib.spigot.configuration.serializers.YMLSerializer;
import me.sharkz.milkalib.spigot.hooks.SpigotHookerManager;
import me.sharkz.milkalib.spigot.listeners.UIListener;
import me.sharkz.milkalib.spigot.logger.SpigotLogger;
import me.sharkz.milkalib.spigot.scheduler.BukkitSchedulerAdapter;
import me.sharkz.milkalib.spigot.ui.MilkaUI;
import me.sharkz.milkalib.spigot.ui.UIManager;
import me.sharkz.milkalib.spigot.utils.misc.ReflectionUtil;
import org.apache.commons.lang.Validate;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 */
@Getter
public abstract class SpigotPlugin extends JavaPlugin implements MilkaPlugin {

    private final SpigotLogger milkaLogger;
    private final BukkitSchedulerAdapter schedulerAdapter;
    private final SpigotHookerManager hookerManager;
    private final AdaptersManager adaptersManager;
    private final SpigotCommandManager commandsManager;
    private final ConfigurationManager configurationManager;
    private final UIManager uiManager;
    private final TranslationManager translationManager;
    private DependencyManager dependencyManager;

    public SpigotPlugin() {
        this.milkaLogger = new SpigotLogger(this);

        this.schedulerAdapter = new BukkitSchedulerAdapter(this);
        this.hookerManager = new SpigotHookerManager(this);
        this.adaptersManager = new AdaptersManager();
        this.configurationManager = new ConfigurationManager(this);
        this.translationManager = new TranslationManager(this);
        this.commandsManager = new SpigotCommandManager(this);
        this.uiManager = new UIManager(this);
        if (!ReflectionUtil.hasClass("org.bukkit.plugin.java.LibraryLoader")) {
            debug("Loading dependencies manager...");
            PluginClassLoader pluginClassLoader = new ReflectionClassLoader(this);
            this.dependencyManager = new DependencyManager(this, pluginClassLoader);
        }
    }

    /**
     * Initialize preinstalled services
     */
    protected void init() {
        initDataFolder();

        /* Translations */
        initTranslationsFolder();
        translationManager.reloadTranslations();

        /* Commands Adapters */
        adaptersManager.register(
                IntegerAdapter.class,
                DoubleAdapter.class,
                BooleanAdapter.class,
                StringAdapter.class,
                WorldAdapter.class,
                EntityTypeAdapter.class,
                PlayerAdapter.class,
                OfflinePlayerAdapter.class,
                MaterialAdapter.class
        );

        /* UI */
        new UIListener(this);
    }

    /**
     * Unload preinstalled services
     */
    public void unload() {
        /* UI */
        getUiManager().unload();

        /* Scheduler */
        unloadScheduler();
    }

    @Override
    public void onReload() {
        /* Translations */
        translationManager.reloadTranslations();

        /* Configurations */
        configurationManager.loadAll();
    }

    public void registerTranslation(Class<? extends TranslationPack> clazz) {
        translationManager.register(clazz);
    }

    public void registerCommand(SpigotCommand command) throws CommandRegistrationException {
        commandsManager.register(command);
    }

    public void registerCommand(Class<? extends SpigotCommand> command, Object... constructorArguments) {
        commandsManager.register(command, constructorArguments);
    }

    public void unregisterCommand(SpigotCommand command) throws CommandUnregistrationException {
        commandsManager.unregister(command);
    }

    public void registerUI(Class<? extends MilkaUI> uiClass, Object... objects) {
        uiManager.register(uiClass, objects);
    }

    public void registerHooker(Class<? extends PluginHooker> hookerClass, Object... objects) {
        hookerManager.register(hookerClass, objects);
    }

    public void registerSerializer(Class<? extends YMLSerializer<?>> serializer, Object... objects) {
        configurationManager.getSerializersManager().register(serializer, objects);
    }

    public void registerSerializers(Class<? extends YMLSerializer<?>>... serializers) {
        configurationManager.getSerializersManager().register(serializers);
    }

    public boolean isHooked(Class<? extends PluginHooker> hookerClass) {
        return hookerManager.isHooked(hookerClass);
    }

    public void loadDependencies(Dependency... dependencies) {
        if (Objects.isNull(dependencyManager)) return;
        try {
            dependencyManager.loadDependencies(dependencies);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void info(String message) {
        getMilkaLogger().info(message);
    }

    public void warn(String message) {
        getMilkaLogger().warn(message);
    }

    public void success(String message) {
        getMilkaLogger().success(message);
    }

    public void debug(String message) {
        getMilkaLogger().debug(message);
    }

    public void error(String message) {
        getMilkaLogger().error(message);
    }

    public abstract String getPrefix();

    @Override
    public void disable() {
        getServer().getPluginManager().disablePlugin(this);
    }

    /**
     * This method provides fast access to the plugin that has {@link
     * #getProvidingPlugin(Class) provided} the given plugin class, which is
     * usually the plugin that implemented it.
     * <p>
     * An exception to this would be if plugin's jar that contained the class
     * does not extend the class, where the intended plugin would have
     * resided in a different jar / classloader.
     *
     * @param clazz the class desired
     * @return the plugin that provides and implements said class
     * @throws IllegalArgumentException if clazz is null
     * @throws IllegalArgumentException if clazz does not extend {@link
     *                                  SpigotPlugin}
     * @throws IllegalStateException    if clazz was not provided by a plugin,
     *                                  for example, if called with
     *                                  <code>JavaPlugin.getPlugin(JavaPlugin.class)</code>
     * @throws IllegalStateException    if called from the static initializer for
     *                                  given JavaPlugin
     * @throws ClassCastException       if plugin that provided the class does not
     *                                  extend the class
     */
    public static <T extends SpigotPlugin> T getMilkaPlugin(Class<T> clazz) {
        Validate.notNull(clazz, "Null class cannot have a plugin");
        if (!SpigotPlugin.class.isAssignableFrom(clazz))
            throw new IllegalArgumentException(clazz + " does not extend " + SpigotPlugin.class);
        return clazz.cast(clazz);
    }
}
