package me.sharkz.milkalib.spigot.ui;

import me.sharkz.milkalib.spigot.SpigotPlugin;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author Roch Blondiaux
 */
public abstract class DynamicUI extends MilkaUI {

    private final long refreshRate;

    public DynamicUI(SpigotPlugin plugin, long refreshRate) {
        super(plugin);
        this.refreshRate = refreshRate;
    }

    public DynamicUI(SpigotPlugin plugin) {
        super(plugin);
        this.refreshRate = 20;
    }

    @Override
    public void open(Player player, Object[] objects) {
        runRepeatedSync(new BukkitRunnable() {
            @Override
            public void run() {
                display(player, objects);
                if (getInventory().getViewers().contains(player))
                    this.cancel();
            }
        }, 0L, refreshRate);
    }

    /**
     * Method called each time ui refresh (1s)
     *
     * @param player
     * @param objects
     */
    abstract void display(Player player, Object[] objects);
}
