package me.sharkz.milkalib.spigot.commands.integrated;

import me.sharkz.milkalib.core.commands.CommandResult;
import me.sharkz.milkalib.core.commands.SenderType;
import me.sharkz.milkalib.spigot.SpigotPlugin;
import me.sharkz.milkalib.spigot.commands.SpigotCommand;
import me.sharkz.milkalib.spigot.ui.MilkaUI;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class UICommand extends SpigotCommand {

    private final Class<? extends MilkaUI> uiClass;
    private final Object[] objects;

    public UICommand(SpigotPlugin plugin, String key, String description, Class<? extends MilkaUI> uiClass, Object... objects) {
        super(plugin, key);

        setDescription(description);
        setAllowedSenderType(SenderType.PLAYER);

        this.uiClass = uiClass;
        this.objects = objects;
    }

    public UICommand(SpigotPlugin plugin, String key, String description, String permission, Class<? extends MilkaUI> uiClass, Object... objects) {
        super(plugin, key);

        setDescription(description);
        setPermission(permission);
        setAllowedSenderType(SenderType.PLAYER);

        this.uiClass = uiClass;
        this.objects = objects;
    }

    @Override
    protected CommandResult execute(CommandSender sender, Object[] args) {
        openUI((Player) sender, uiClass, objects);
        return CommandResult.SUCCESS;
    }
}
