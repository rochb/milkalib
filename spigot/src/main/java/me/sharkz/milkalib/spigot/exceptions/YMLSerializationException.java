package me.sharkz.milkalib.spigot.exceptions;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class YMLSerializationException extends Exception {

    public YMLSerializationException(String message) {
        super(message);
    }
}
