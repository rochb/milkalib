package me.sharkz.milkalib.spigot.commands.adapters;

import me.sharkz.milkalib.core.adapters.MilkaAdapter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PlayerAdapter implements MilkaAdapter<Player> {

    @Override
    public Optional<Player> parse(String data) {
        try {
            Player player = Bukkit.getPlayer(data);
            if (Objects.isNull(player) || !player.isValid() || !player.isOnline()) return Optional.empty();
            return Optional.of(player);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public Collection<String> getSuggestions() {
        return Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
    }
}
